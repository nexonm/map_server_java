/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.logging.NxLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author David.Vallejo
 */
public class ColumnSpec {

    private static final String[] rawValidSourceTypes = {"DEVELOPER", "SDK", "DEVELOPER|SDK", "SDK|DEVELOPER"};
    public static final List<String> validSourceTypes = Arrays.asList(rawValidSourceTypes);

    private static final String TAG = "ColumnSpec";
    
    private String columnName;
    private String shortname;
    private boolean required;
    private String transportType;
    private String valueSource;
    private boolean supportedColumnSpec;
    
    private List<String> errors;
    private ParamRestriction restriction;
                    
    //TODO: make the Exception error catching nicer in the error messages

    ColumnSpec(JSONObject rawColumnSpec) {
        errors = new ArrayList<String>();
        try {
            columnName = rawColumnSpec.getString(JsonKeys.PARAMETER_COLUMN_NAME);
            shortname = rawColumnSpec.getString(JsonKeys.PARAMETER_SHORTNAME);
            required = rawColumnSpec.getBoolean(JsonKeys.PARAMETER_REQUIRED);
            transportType = rawColumnSpec.getString(JsonKeys.PARAMETER_TRANSPORT_TYPE);
            valueSource = rawColumnSpec.getString(JsonKeys.PARAMETER_VALUE_SOURCE);

            if(validSourceTypes.contains(valueSource)){
                supportedColumnSpec = true;
                if (transportType.equalsIgnoreCase("string")) {
                    restriction = new StringRestriction(rawColumnSpec.getJSONObject(JsonKeys.PARAMETER_RESTRICTIONS));
                } else if (transportType.equalsIgnoreCase("boolean") || transportType.equalsIgnoreCase("bool")) {
                    restriction = new BooleanRestriction();
                } else {
                    restriction = new NumberRestriction(transportType, rawColumnSpec.getJSONObject(JsonKeys.PARAMETER_RESTRICTIONS));
                }

                if (columnName == null) {
                    errors.add("Missing Column Name: " + rawColumnSpec.toString());
                }

                if (shortname == null) {
                    errors.add("Missing Shortname: " + rawColumnSpec.toString());
                }

                if (transportType == null) {
                    errors.add("Missing transportType: " + rawColumnSpec.toString());
                }
            }else{
                supportedColumnSpec = false;
            }

            
        } catch (Exception ex) {
            NxLogger.error(TAG, "[ColumnSpec] Error occurred for while creating column spec for %s", rawColumnSpec.toString());
            errors.add(ex.getMessage());
        }
        
    }

    boolean isValid() {
        return errors.isEmpty();
    }

    boolean isSupportedColumnSpec(){
        return supportedColumnSpec;
    }

    protected String getColumnName() {
        return columnName;
    }

    public String getShortname() {
        return shortname;
    }

    protected boolean isRequired() {
        return required;
    }

    protected String getTransportType() {
        return transportType;
    }

    protected String getValueSource() {
        return valueSource;
    }

    protected List<String> getErrors() {
        return errors;
    }

    protected boolean validateValue(Object value) {
        return restriction.validateValue(value);
    }
}
