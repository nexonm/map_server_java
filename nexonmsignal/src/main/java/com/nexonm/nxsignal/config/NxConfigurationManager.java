/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.NxSignalOptions;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.nexonm.nxsignal.utils.NxUtils.isNullOrEmptyString;

/**
 * Created by David.Vallejo on 2/3/16.
 */
public class NxConfigurationManager {
    
    private static final String TAG = "NxConfigurationManager";
    private static final String CONFIG_VERSION = "0.0.0.0";

    private NxConfiguration configuration = null;

    private boolean isReady = false;

    private static NxConfigurationManager sharedInstance = null;

    public static synchronized NxConfigurationManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxConfigurationManager();
        }
        return sharedInstance;
    }

    private NxConfigurationManager(){
        configuration = null;
    }

    public void setup(NxSignalOptions signalOptions) {

        String configString = signalOptions.getConfig();

        String gameId = null;
        String partnerId = null;

        //
        // No config provided.
        //
        if(isNullOrEmptyString(configString)) {
            NxLogger.info(TAG, "[setup] no config passed-in. Config will be created programmatically");
        }
        //
        // Config string provided, let's parse it.
        //
        else {
            NxLogger.info(TAG, "[setup] use the passed-in config");

            try {
                JSONObject configJson = new JSONObject(configString);

                JSONObject analyticsConfiguration = configJson.getJSONObject(JsonKeys.ANALYTICS_CONFIGURATION);

                gameId = (String)analyticsConfiguration.get(JsonKeys.ANALYTICS_APP_ID);
                partnerId = (String)analyticsConfiguration.get(JsonKeys.ANALYTICS_PARTNER_ID);

            } catch (JSONException e) {
                NxLogger.error(TAG, "[setup] Failed to convert passed-in config to JSONObject. %s", e.getMessage());
            }
        }

        //
        // In no gameId was found in the config string, check if the gameId has been provided from
        // Toy in the options (LOWER PRIORITY).
        //
        if(isNullOrEmptyString(gameId) && !isNullOrEmptyString(signalOptions.getGame_id())) {
            // If so, use the options value
            gameId = signalOptions.getGame_id();
        }

        if(!isNullOrEmptyString(gameId)) {

            //
            // If no partnerId was found in the config string, check if the partnerId has been
            // provided from Toy in the options (LOWER PRIORITY).
            //
            if(isNullOrEmptyString(partnerId) && !isNullOrEmptyString(signalOptions.getPartner_id())) {
                // If so, just use the options value
                partnerId = signalOptions.getPartner_id();
            }

            // If it is still null, then generate one on the fly from the given gameId.
            if (isNullOrEmptyString(partnerId)) {
                partnerId = NxUtils.createPartnerIdForAppId(gameId);
            }

            try {
                //
                // Create new JSON object for config from scratch
                //
                JSONObject configJson = new JSONObject();

                JSONObject analyticsConfigurationJson = new JSONObject();
                analyticsConfigurationJson.put(JsonKeys.ANALYTICS_PARTNER_ID, partnerId);
                analyticsConfigurationJson.put(JsonKeys.ANALYTICS_APP_ID, gameId);
                analyticsConfigurationJson.put(JsonKeys.ANALYTICS_ERROR_EVENT_CHANNEL_ENABLED, true);
                analyticsConfigurationJson.put(JsonKeys.LOGGING_MAX_LOG_SIZE, 10240);
                analyticsConfigurationJson.put(JsonKeys.LOGGING_REMOTE_LOGGING, false);
                analyticsConfigurationJson.put(JsonKeys.ANALYTICS_CONFIG_VERSION, CONFIG_VERSION);

                configJson.put(JsonKeys.ANALYTICS_CONFIGURATION, analyticsConfigurationJson);

                JSONObject apiServerUrls = new JSONObject();
                String apiUrlProd = "https://api.analytics.nexonm.com/v1/" + gameId + "/";
                String apiUrlDev = "https://api-dev.analytics.nexonm.com/v1/" + gameId + "/";

                apiServerUrls.put("prod", apiUrlProd);
                apiServerUrls.put("dev", apiUrlDev);

                analyticsConfigurationJson.put(JsonKeys.ANALYTICS_API_SERVER_URL, apiServerUrls);

                //
                // Create nxsignal adapter config
                //
                JSONObject nxSignalAdapterJSON = new JSONObject();

                nxSignalAdapterJSON.put(JsonKeys.ADAPTER_IDENTIFIER, "nxsignal");
                nxSignalAdapterJSON.put(JsonKeys.ADAPTER_ENABLED, true);
                nxSignalAdapterJSON.put(JsonKeys.LOGGING_LEVEL, signalOptions.getLogLevel());

                JSONObject dataJSON = new JSONObject();
                dataJSON.put(JsonKeys.ANALYTICS_HIGH_PRIORITY_THRESHOLD, 10);
                dataJSON.put(JsonKeys.ANALYTICS_BATCH_COMPRESSION_LEVEL, -1);
                dataJSON.put(JsonKeys.ANALYTICS_BATCH_COMPRESSION_ENABLED, false);
                dataJSON.put(JsonKeys.ANALYTICS_HIGH_PRIORITY_EVENTS_DROPPABLE, true);
                dataJSON.put(JsonKeys.ANALYTICS_MAX_EVENTS_PER_BATCH_SEND, 30);
                dataJSON.put(JsonKeys.ANALYTICS_EVENT_STORAGE_LIMIT, 10240);

                JSONObject eventProcessingTriggersJSON = new JSONObject();
                eventProcessingTriggersJSON.put(JsonKeys.ANALYTICS_SEND_COOLDOWN, 1);
                eventProcessingTriggersJSON.put(JsonKeys.ANALYTICS_NUMBER_OF_EVENTS_IN_QUEUE_FLUSH_THRESHOLD, 20);
                eventProcessingTriggersJSON.put(JsonKeys.ANALYTICS_MAX_ACCEPTABLE_QUEUE_SIZE, 100);
                eventProcessingTriggersJSON.put(JsonKeys.ANALYTICS_TIME_TILL_BATCH_SENDING_TO_SERVER, 60);

                JSONArray triggerEventsJSON = new JSONArray();
                triggerEventsJSON.put(JsonKeys.EVENT_NAME_LAUNCH);
                triggerEventsJSON.put(JsonKeys.EVENT_NAME_REVENUE);

                eventProcessingTriggersJSON.put(JsonKeys.ANALYTICS_TRIGGER_EVENTS, triggerEventsJSON);

                dataJSON.put(JsonKeys.ANALYTICS_EVENT_PROCESSING_TRIGGERS, eventProcessingTriggersJSON);

                nxSignalAdapterJSON.put(JsonKeys.ADAPTER_DATA, dataJSON);

                //
                // Create nxsignal adapter config
                //
                JSONObject nxAdjustAdapterJSON = new JSONObject();

                nxAdjustAdapterJSON.put(JsonKeys.ADAPTER_IDENTIFIER, "adjust");
                nxAdjustAdapterJSON.put(JsonKeys.ADAPTER_ENABLED, true);
                nxAdjustAdapterJSON.put(JsonKeys.LOGGING_LEVEL, signalOptions.getLogLevel());

                JSONArray adapters = new JSONArray();
                adapters.put(nxSignalAdapterJSON);
                adapters.put(nxAdjustAdapterJSON);

                configJson.put(JsonKeys.ADAPTERS, adapters);

                //
                // Create a new instance of client analytics config from the given JSON
                //
                NxConfiguration newConfiguration = new NxConfiguration(configJson);

                //
                // If the analytics config is invalid, the SDK is useless then and in a not ready
                // state forever.
                //
                if (!newConfiguration.isValid()) {
                    NxLogger.error(TAG, "[setup] Config is not valid");
                    List<String> errors = newConfiguration.getErrors();

                    for (int i = 0; i < errors.size(); i++) {
                        NxLogger.verbose(TAG, errors.get(i));
                    }

                    configuration = null;

                } else {

                    configuration = newConfiguration;

                    NxLogger.info(TAG, "[setup] Config loaded.");
                }
            } catch (JSONException e) {
                NxLogger.error(TAG, "[setup] Error creating config JSONObject. %s", e.getMessage());
            } catch (NullPointerException e) {
                NxLogger.error(TAG, "[setup] Config is null. %s", e.getMessage());
            }
        } else {
            //
            // Cannot find a gameId anywhere, actually we are screwed and we should never reach this point...
            //
            NxLogger.fatal(TAG, "[setup] Cannot retrieve gameId anywhere!");
        }

        if(configuration != null) {

            isReady = true;

            NxLogger.info(TAG, "[setup] NxConfigurationManager setup finished. Manager is ready and configuration is valid.");
        } else {

            isReady = false;

            NxLogger.fatal(TAG, "[setup] NxConfigurationManager is NOT ready. Configuration is null.");
        }
    }
    
    public NxConfiguration getConfiguration() {
        return configuration;
    }

    public boolean isReady() {
        return isReady;
    }
}
