package com.nexonm.nxsignal.config.enums;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public enum ValueSource {

    ValueSourceSDK("sdk",1),
    ValueSourceConfigFile("config_file",2),
    ValueSourceEventCreated("event_created",3),
    ValueSourceSessionStart("sesssion_start",4),
    ValueSourceAppInput("app_input",5);


    private int intValue;
    private String strValue;

    private ValueSource(String neoStringValue, int neoIntValue){
        intValue = neoIntValue;
        strValue = neoStringValue;
    }

    @Override
    public String toString(){
        return strValue;
    }
    
    public int toInt(){
        return intValue;
    }

}