package com.nexonm.nxsignal.config;


import com.nexonm.nxsignal.logging.NxLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by David.Vallejo on 2/2/16.
 */
public class NxConfiguration {

    private List<String> configErrors;
    private NxAnalyticsConfiguration analyticsConfiguration;
    private Map<String, NxAdapterConfiguration> adapterConfigurations;

    private static final String TAG = "NxConfiguration";

    NxConfiguration(JSONObject jsonConfig) {
        
        configErrors = new ArrayList();
        
        try {
            //
            // Parse the SDK analytics configuration
            //
            JSONObject clientJson = jsonConfig.getJSONObject(JsonKeys.ANALYTICS_CONFIGURATION);
            analyticsConfiguration = new NxAnalyticsConfiguration(clientJson);

            // Parse all the adapter configurations
            JSONArray adapterConfigurationsJson = jsonConfig.getJSONArray(JsonKeys.ADAPTERS);
            parseAdapterConfigurations(adapterConfigurationsJson);

        } catch (JSONException ex) {
            Logger.getLogger(NxConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void parseAdapterConfigurations(JSONArray adapterConfigurationsJson) {
        adapterConfigurations = new HashMap<String, NxAdapterConfiguration>();

        // No adapters listed
        if(adapterConfigurationsJson.length() == 0) {
            return;
        }

        for(int adapterIndex = 0; adapterIndex < adapterConfigurationsJson.length(); adapterIndex++) {
            try {
                JSONObject adapterRawJson = adapterConfigurationsJson.getJSONObject(adapterIndex);
                NxAdapterConfiguration adapterConfig = new NxAdapterConfiguration(adapterRawJson);
                adapterConfigurations.put(adapterConfig.getAdapterIdentifier(), adapterConfig);
            } catch (JSONException e) {
                NxLogger.error(TAG, "[parseAdapterConfigurations] " + e.getMessage());
            }
        }
    }

    public NxAnalyticsConfiguration getAnalyticsConfiguration() {
        return analyticsConfiguration;
    }

    public boolean isValid(){
        return (configErrors.isEmpty() && analyticsConfiguration.isValid());
    }

    public Map<String, NxAdapterConfiguration> getAdapterConfigurations() {
        return adapterConfigurations;
    }

    public NxAdapterConfiguration getAdapterConfiguration(String key){
        return adapterConfigurations.get(key);
    }

    public List<String> getErrors(){
        return configErrors;
    }
}
