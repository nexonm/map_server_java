/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;

import java.util.List;
import java.util.Map;

/**
 *
 * @author David.Vallejo
 */
public class EventSpec {
    private int priority;
    private List<String> eventColumnList;
    private String name;
    private Map<String, ColumnSpec> columnSpecMap;
    
    public EventSpec(String name,int priority,List<String> eventColumnList,  Map<String, ColumnSpec> columnSpecMap){
        this.priority = priority;
        this.eventColumnList = eventColumnList;
        this.name = name;
        this.columnSpecMap = columnSpecMap;
    }

    public int getPriority() {
        return priority;
    }

    public List<String> getEventColumnList() {
        return eventColumnList;
    }

    public Map<String, ColumnSpec> getColumnSpecMap(){
        return  this.columnSpecMap;
    }
    
    
    
}
