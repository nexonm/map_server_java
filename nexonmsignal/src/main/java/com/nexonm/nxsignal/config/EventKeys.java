/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;

/**
 *
 * @author David.Vallejo
 */
public class EventKeys {
    public static final String EVENT_TYPE =  "event_type";
    
    public static final String RESTRICTION_PATTERN = "pattern";
    public static final String RESTRICTION_LENGTH = "length";
    public static final String RESTRICTION_LENGTH_MIN = "length_min";
    public static final String RESTRICTION_VALID_VALUES = "valid_values";
    
    public static final String RESTRICTION_RANGE = "range";
    public static final String RESTRICTION_RANGE_MAX_EXCLUSIVE = "range_max_exclusive";
    public static final String RESTRICTION_RANGE_MIN_EXCLUSIVE = "range_min_exclusive";
}
