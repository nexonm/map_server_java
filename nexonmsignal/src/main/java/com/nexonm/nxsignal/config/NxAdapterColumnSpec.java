package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.logging.NxLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincentfang on 2/22/16.
 */
public class NxAdapterColumnSpec {

    private static final String TAG = "NxAdapterColumnSpec";

    private String parameterName;
    private boolean required;
    private String dataType;
    private String parameterValueSource;
    private String overwriteParameterValue;
    private ParamRestriction restriction;
    private List<String> errors;

    public NxAdapterColumnSpec(JSONObject adapterParametersJson) {

        errors = new ArrayList<String>();

        try {
            parameterName = adapterParametersJson.getString(JsonKeys.ADAPTER_PARAMETER_NAME);
            required = adapterParametersJson.getBoolean(JsonKeys.ADAPTER_PARAMETER_REQUIRED);
            dataType = adapterParametersJson.getString(JsonKeys.ADAPTER_PARAMETER_DATA_TYPE);

            // Adapter parameter can either get its value from a source or from an overwrite value, not both
            if(adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_VALUE_SOURCE)) {
                parameterValueSource = adapterParametersJson.getString(JsonKeys.ADAPTER_PARAMETER_VALUE_SOURCE);
                overwriteParameterValue = null;
            } else if(adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_OVERWRITE_PARAMETER_VALUE)) {
                parameterValueSource = null;
                overwriteParameterValue = adapterParametersJson.getString(JsonKeys.ADAPTER_PARAMETER_OVERWRITE_PARAMETER_VALUE);
            }

            // If both value source and overwrite value are filled out or both are left blank,
            // something went wrong with the config
            if((adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_VALUE_SOURCE) == true &&
                    adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_OVERWRITE_PARAMETER_VALUE) == true) ||
                    (adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_VALUE_SOURCE) == false) &&
                            adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_OVERWRITE_PARAMETER_VALUE) == false) {
                NxLogger.warn(TAG, "[NxAdapterColumnSpec] Parameter " + parameterName
                        + " has both parameter value source and overwrite parameter value filled out. Using overwrite value.");
            }

            // Adapter parameter restrictions
            //if(adapterParametersJson.has(JsonKeys.ADAPTER_PARAMETER_RESTRICTIONS)) {
            //    JSONObject restrictionsJson = adapterParametersJson.getJSONObject(JsonKeys.ADAPTER_PARAMETER_RESTRICTIONS);

            JSONObject restrictionsJson = new JSONObject();
                if (dataType.equalsIgnoreCase("string")) {
                    restriction = new StringRestriction(restrictionsJson);
                } else if(dataType.equalsIgnoreCase("boolean") || dataType.equalsIgnoreCase("bool")) {
                    restriction = new BooleanRestriction();
                } else {
                    restriction = new NumberRestriction(dataType, restrictionsJson);
                }
            //}
        } catch (JSONException e) {
            NxLogger.error(TAG, "[NxAdapterColumnSpec] Exception occurred during constructor of adapter column spec %s", e.getMessage());
        }
    }

    public boolean doesValueNeedToBeSet() {
        return (overwriteParameterValue == null);
    }

    public String getParameterName() {
        return parameterName;
    }

    public boolean isRequired() {
        return required;
    }

    public String getDataType() {
        return dataType;
    }

    public String getParameterValueSource() {
        return parameterValueSource;
    }

    public String getOverwriteParameterValue() {
        return overwriteParameterValue;
    }

    public List<String> getErrors() {
        return errors;
    }

    boolean validateValue(Object value) {
        try{

            if(restriction == null) {
                NxLogger.error(TAG, "[validateValue] Restriction for parameter %s is null", parameterName);
            }

            return restriction.validateValue(value);
        } catch (Exception ex) {
            errors.add(ex.getMessage());
            NxLogger.error(TAG, "[validateValue] Exception occurred, %s", ex.getMessage());
        }
        return false;
    }
}
