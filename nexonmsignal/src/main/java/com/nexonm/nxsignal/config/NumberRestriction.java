/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;


import com.nexonm.nxsignal.logging.NxLogger;

import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author David.Vallejo
 */
public class NumberRestriction implements ParamRestriction{

    private static final String TAG = "NumberRestriction";
    private static final String FLOAT_TYPE = "float";
    private static final String INTEGER_TYPE = "integer";
    private static final String LONG_TYPE = "long";
    private static final String SHORT_TYPE = "short";
       
    private Set<Number> validValues;
    
    private boolean rangeMinExclusive = false;
    private boolean rangeMaxExclusive = false;
    
    private Number min;
    private Number max;
    
    private String transportType;

    NumberRestriction(String transportType, JSONObject jsonObject) throws JSONException, NumberFormatException {

        this.transportType = transportType;

        if (jsonObject.has(EventKeys.RESTRICTION_VALID_VALUES)) {
            JSONArray values = jsonObject.getJSONArray(EventKeys.RESTRICTION_VALID_VALUES);

            validValues = new HashSet<Number>();

            for (int i = 0; i < values.length(); i++) {
                Number currentValue = convertType(transportType, values.get(i).toString());
                validValues.add(currentValue);
            }
        }
        
        if(jsonObject.has(EventKeys.RESTRICTION_RANGE)){
            JSONArray range = jsonObject.getJSONArray(EventKeys.RESTRICTION_RANGE);
            
            if(range != null && range.length()>0){
                min = convertType(transportType, range.get(0).toString());
                if(range.length()>1){
                    max = convertType(transportType, range.get(1).toString());
                }
            }

            // TODO if max is less than min
            
            if(jsonObject.has(EventKeys.RESTRICTION_RANGE_MIN_EXCLUSIVE)){
                rangeMinExclusive = jsonObject.getBoolean(EventKeys.RESTRICTION_RANGE_MIN_EXCLUSIVE);
            }
            
            if(jsonObject.has(EventKeys.RESTRICTION_RANGE_MAX_EXCLUSIVE)){
                rangeMaxExclusive = jsonObject.getBoolean(EventKeys.RESTRICTION_RANGE_MAX_EXCLUSIVE);
            }
            
        }
    }
    
    private static Number convertType(String transportType, String value) throws NumberFormatException, UnsupportedOperationException {
        Number result;
        if(transportType.equalsIgnoreCase(FLOAT_TYPE)) {
            result = new Float(value);
        } else if(transportType.equalsIgnoreCase(INTEGER_TYPE)) {
            result = new Integer(value);
        } else if(transportType.equalsIgnoreCase(LONG_TYPE)) {
            result = new Long(value);
        } else if(transportType.equalsIgnoreCase(SHORT_TYPE)) {
            result = new Short(value);
        } else {
            NxLogger.error(TAG, "[convertType] Unrecognized transport type for NumberRestriction: %s", transportType);
            throw new UnsupportedOperationException("Unrecognized transport type for NumberRestriction: " + transportType);
        }
        
        return result;
    }

    private static Integer compareNumbers(String transportType, Number value1, Number value2){
        if(transportType.equalsIgnoreCase(FLOAT_TYPE)) {
            Float temp1 = new Float(value1.floatValue());
            Float temp2 = new Float(value2.floatValue());
            return temp1.compareTo(temp2);
        } else if(transportType.equalsIgnoreCase(INTEGER_TYPE)) {
            Integer temp1 = new Integer(value1.intValue());
            Integer temp2 = new Integer(value2.intValue());
            return temp1.compareTo(temp2);
        } else if(transportType.equalsIgnoreCase(LONG_TYPE)) {
            Long temp1 = new Long(value1.longValue());
            Long temp2 = new Long(value2.longValue());
            return temp1.compareTo(temp2);
        } else if(transportType.equalsIgnoreCase(SHORT_TYPE)) {
            Short temp1 = new Short(value1.shortValue());
            Short temp2 = new Short(value1.shortValue());
            return temp1.compareTo(temp2);
        }

        // Error could only occur if we were adding new supported types and forgot to add them in here too
        NxLogger.error(TAG, "[compareNumbers] TransportType not supported: %s", transportType);
        return null;
    }
    
    @Override
    public boolean validateValue(Object value) {

        //NxLogger.verbose(TAG, "[validateValue] Number Restriction.");

        
        if(value == null) {
            NxLogger.error(TAG, "[validateValue] Value is null");
            return false;
        }

        if(!(value instanceof Number)) {
            NxLogger.error(TAG, "[validateValue] Value is not a number");
            return false;
        }

        if(this.validValues != null) {
            return validValues.contains((Number)value);
        }

        if(min != null) {
            Integer compMin = compareNumbers(transportType, min, (Number)value);

            if(compMin == null) {
                return false;
            }

            if(this.rangeMinExclusive) {
                if(compMin>=0){
                    NxLogger.error(TAG, "[validateValue] Failed %s is not greater than %s",
                            ((Number)value).toString(), min.toString());
                    return false;
                }
            } else {
                if(compMin>0) {
                    NxLogger.error(TAG, "[validateValue] Failed %s is not greater than or equal to %s",
                            ((Number)value).toString(), min.toString());
                    return false;
                }
            }

        }

        if(max != null) {
            Integer compMax = compareNumbers(transportType, max, (Number)value);

            if(compMax == null) {
                return false;
            }

            if(this.rangeMaxExclusive) {
                if(compMax<=0){
                    NxLogger.error(TAG, "[validateValue] Failed %s is not less than %s",
                            ((Number)value).toString(), max.toString());
                    return false;
                }
            } else {
                if(compMax<0) {
                    NxLogger.error(TAG, "[validateValue] Failed %s is not less than or equal to %s",
                            ((Number)value).toString(), max.toString());
                    return false;
                }
            }

        }
            
        return true;
    }

}
