package com.nexonm.nxsignal.config;

/**
 * Created by David.Vallejo on 2/3/16.
 */
public class JsonKeys {

    // SDK VERSION
    public static final String SDK_VERSION = "3.0.5";

    // JSON key fields for analytics_configuration
    public static final String ANALYTICS_CONFIGURATION =  "analytics_configuration";
    public static final String ANALYTICS_PARTNER_ID =  "partner_id";
    public static final String ANALYTICS_APP_ID =  "app_id";
    public static final String ANALYTICS_API_SERVER_URL =  "api_server_url";
    public static final String ANALYTICS_CONFIG_SERVER_URL =  "config_server_url";
    public static final String ANALYTICS_PREFERRED_DATA_FORMAT =  "preferred_data_format";
    //public static final String ANALYTICS_SERVER_MODE =  "server_mode";
    public static final String ANALYTICS_SDK_ACTIVATED =  "sdk_activated";
    public static final String ANALYTICS_SDK_ENVIRONMENT =  "sdk_environment";
    public static final String ANALYTICS_EVENT_PROCESSING_TRIGGERS =  "event_processing_triggers";
    public static final String ANALYTICS_TRIGGER_EVENTS =  "trigger_events";
    public static final String ANALYTICS_NUMBER_OF_EVENTS_IN_QUEUE_FLUSH_THRESHOLD =  "number_of_events_in_queue_flush_threshold";
    public static final String ANALYTICS_TIME_TILL_BATCH_SENDING_TO_SERVER =  "time_till_batch_sending_to_server";
    public static final String ANALYTICS_MAX_ACCEPTABLE_QUEUE_SIZE =  "max_acceptable_queue_size";
    public static final String ANALYTICS_SEND_COOLDOWN =  "send_cooldown";
    public static final String ANALYTICS_EVENT_STORAGE_LIMIT =  "event_storage_limit";
    public static final String ANALYTICS_EVENT_STORAGE_LIMIT_TYPE =  "event_storage_limit_type";
    public static final String ANALYTICS_MAX_EVENTS_PER_BATCH_SEND =  "max_events_per_batch_send";
    public static final String ANALYTICS_BATCH_COMPRESSION_ENABLED =  "batch_compression_enabled";
    public static final String ANALYTICS_BATCH_COMPRESSION_LEVEL =  "batch_compression_level";
    public static final String ANALYTICS_MAX_EVENTS_DROPPABLE =  "max_events_droppable";
    public static final String ANALYTICS_HIGH_PRIORITY_EVENTS_DROPPABLE =  "high_priority_events_droppable";
    public static final String ANALYTICS_HIGH_PRIORITY_THRESHOLD =  "high_priority_threshold";
    public static final String ANALYTICS_ERROR_EVENT_CHANNEL_ENABLED = "error_event_channel_enabled";
    public static final String ANALYTICS_CONFIG_VERSION =  "config_version";

    // JSON key fields for logging
    public static final String LOGGING_LEVEL =  "logging_level";
    public static final String LOGGING_REMOTE_LOGGING =  "remote_logging";
    public static final String LOGGING_MAX_LOG_SIZE =  "max_log_size";

    // JSON key fields for adapters
    public static final String ADAPTERS =  "adapters";
    public static final String ADAPTER_IDENTIFIER =  "adapter_identifier";
    public static final String ADAPTER_ENABLED =  "enabled";
    public static final String ADAPTER_DATA =  "data";
    public static final String ADAPTER_IOS_DATA = "ios_data";
    public static final String ADAPTER_ANDROID_DATA = "android_data";
    public static final String ADAPTER_DATA_ANDROID_PACKAGE = "android_package";
    public static final String ADAPTER_EVENTS_SUPPORTED =  "events_supported";

    // JSON key fields for adapter parameters
    public static final String ADAPTER_PARAMETERS =  "parameters";
    public static final String ADAPTER_PARAMETER_NAME = "parameter_name";
    public static final String ADAPTER_PARAMETER_REQUIRED = "required";
    public static final String ADAPTER_PARAMETER_DATA_TYPE = "data_type";
    public static final String ADAPTER_PARAMETER_VALUE_SOURCE = "parameter_value_source";
    public static final String ADAPTER_PARAMETER_OVERWRITE_PARAMETER_VALUE = "overwrite_parameter_value";
    public static final String ADAPTER_PARAMETER_RESTRICTIONS = "restrictions";
    public static final String ADAPTER_PARAMETER_RESTRICTION_VALID_VALUES = "valid_values";

    // JSON key fields for events
    public static final String EVENTS =  "events";
    public static final String EVENT_PRIORITY =  "priority";

    // JSON key fields for launch event
    public static final String EVENT_NAME_LAUNCH = "launch";
    public static final String EVENT_NAME_SDK_LOGIN = "sdk_login";
    public static final String EVENT_LAUNCH_TYPE = "launch_type";

    // JSON key fields for econ earn/spend event
    public static final String EVENT_NAME_ECON = "econ";
    public static final String EVENT_ECON_TYPE = "econ_type";
    public static final String EVENT_ECON_VIRTUAL_CURRENCY_TYPE = "virtual_currency_type";
    public static final String EVENT_ECON_VIRTUAL_CURRENCY_AMOUNT = "virtual_currency_amount";
    public static final String EVENT_ECON_VIRTUAL_CURRENCY_BALANCE = "virtual_currency_balance";
    public static final String EVENT_ECON_SOURCE = "source";
    public static final String EVENT_ECON_ACTION = "action";
    public static final String EVENT_ECON_TARGET = "target";

    // JSON key fields for revenue event
    public static final String EVENT_NAME_REVENUE = "purchase";
    public static final String EVENT_REVENUE_SKU_DETAILS = "nx_revenue_sku_details";
    public static final String EVENT_REVENUE_RECEIPT = "nx_revenue_receipt";
    public static final String EVENT_REVENUE_STORE_ID = "store_id";
    public static final String EVENT_REVENUE_PAYMENT_TYPE = "payment_type";
    public static final String EVENT_REVENUE_PRICE = "price";
    public static final String EVENT_REVENUE_DISPLAYED_PRICE = "displayed_price";
    public static final String EVENT_REVENUE_PRICE_LOCALE = "price_locale";
    public static final String EVENT_REVENUE_IAP_SKU = "iap_sku";
    public static final String EVENT_REVENUE_RECEIPT_ID = "receipt_id";
    public static final String EVENT_REVENUE_NUMERIC_PRICE = "numeric_price";

    // JSON key fields for econ source event
    public static final String EVENT_NAME_ECON_SOURCE = "econ_source";

    // JSON key fields for econ sink event
    public static final String EVENT_NAME_ECON_SINK = "econ_sink";

    // JSON key fields for funnel event
    public static final String EVENT_NAME_FUNNEL = "funnel";
    public static final String EVENT_FUNNEL_STEP = "funnel_step";
    public static final String EVENT_FUNNEL_FTUE_START = "nx_ftue_start"; // tutorial start
    public static final String EVENT_FUNNEL_FTUE_END = "nx_ftue_end"; // tutorial end


    // JSON key fields for event parameters
    public static final String PARAMETERS =  "parameters";
    public static final String PARAMETERS_FOR_ALL_EVENTS =  "parameters_for_all_events";
    public static final String PARAMETER_COLUMN_NAME =  "column_name";
    public static final String PARAMETER_SHORTNAME =  "shortname";
    public static final String PARAMETER_REQUIRED =  "required";
    public static final String PARAMETER_DATA_TYPE =  "data_type";
    public static final String PARAMETER_VALUE_SOURCE =  "value_source";
    public static final String PARAMETER_RESTRICTIONS =  "restrictions";
    public static final String PARAMETER_LENGTH =  "length";
    public static final String PARAMETER_REGEX_PATTERN =  "pattern";
    public static final String PARAMETER_VALID_VALUES =  "valid_values";
    public static final String PARAMETER_RANGE =  "range";
    public static final String PARAMETER_RANGE_MIN_EXCLUSIVE =  "range_min_exclusive";
    public static final String PARAMETER_RANGE_MAX_EXCLUSIVE =  "range_max_exclusive";
    public static final String PARAMETER_TRANSPORT_TYPE = "transport_type";

    // Special configuration parameter names
    public static final String SPECIAL_PARAMETER_ENV =  "env";

    // JSON key fields for the static common parameters for all events

    public static final String COMMON_PARAMETER_APP_TYPE = "app_type";
    public static final String COMMON_PARAMETER_APP_VERSION = "app_version";
    public static final String COMMON_PARAMETER_APP_LOCALE = "app_locale";
    public static final String COMMON_PARAMETER_NEXON_DEVICE_ID = "nexon_device_id";
    public static final String COMMON_PARAMETER_PLATFORM_DEVICE_ID = "platform_device_id";
    public static final String COMMON_PARAMETER_PLATFORM_DEVICE_ID_TYPE = "platform_device_id_type";
    public static final String COMMON_PARAMETER_DEVELOPER_DEVICE_ID = "developer_device_id";
    public static final String COMMON_PARAMETER_DEVICE_TYPE = "device_type";
    public static final String COMMON_PARAMETER_DEVICE_MAKE = "device_make";
    public static final String COMMON_PARAMETER_DEVICE_MODEL = "device_model";
    public static final String COMMON_PARAMETER_DEVICE_NAME = "device_name";
    public static final String COMMON_PARAMETER_OS_PLATFORM = "os_platform";
    public static final String COMMON_PARAMETER_OS_NAME = "os_name";
    public static final String COMMON_PARAMETER_SDK_VERSION = "sdk_version";
    public static final String COMMON_PARAMETER_SDK_VERSION_CONFIG = "sdk_version_config";
    public static final String COMMON_PARAMETER_SIGNATURE = "signature";
    public static final String COMMON_PARAMETER_DEVELOPER_PLAYER_ID = "developer_player_id";
    public static final String COMMON_PARAMETER_NPSN = "npsn";
    public static final String COMMON_PARAMETER_APP_BUILD_VERSION = "app_build_version";

    // JSON key fields for the dynamic common parameters for all events
    public static final String COMMON_PARAMETER_SESSION_ID = "session_id";
    public static final String COMMON_PARAMETER_LAST_EVENT_SENT_TIMESTAMP = "last_event_sent_timestamp";
    public static final String COMMON_PARAMETER_EVENT_TYPE = "event_name";
    public static final String COMMON_PARAMETER_APPLICATION_TIMESTAMP = "app_ts";
    public static final String COMMON_PARAMETER_BATCH_OFFLINE = "batch_offline";
    public static final String COMMON_PARAMETER_TOY_SERVICE_ID = "toy_service_id";
    public static final String COMMON_PARAMETER_EVENT_ID = "event_id";

    // Adjust Adapter
    public static final String ADAPTER_ADJUST_KEY =  "NxAdjustKey";
    public static final String ADAPTER_ADJUST_ENABLED =  "NxAdjustEnabled";
    public static final String ADAPTER_ADJUST_SANDBOX =  "NxAdjustSandbox";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_BRIDGING = "NxAdjustEventToken_bridging";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_CUSTOM_REVENUE = "NxAdjustEventToken_custom_revenue";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_ECON_SINK = "NxAdjustEventToken_econ_sink";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_ECON_SOURCE = "NxAdjustEventToken_econ_source";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_TUTORIAL_START = "NxAdjustEventToken_tutorial_start";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_TUTORIAL_COMPLETE = "NxAdjustEventToken_tutorial_complete";
    public static final String ADAPTER_ADJUST_EVENT_TOKEN_NAME_FIRST_PURCHASE = "NxAdjustEventToken_first_purchase";
}
