package com.nexonm.nxsignal.config.enums;

/**
 * Created by David.Vallejo on 2/3/16.
 */
public enum EventStorageLimitType {
    LimitTypeEventCount,
    LimitTypeFileSize,
    LimitTypeNone;
}
