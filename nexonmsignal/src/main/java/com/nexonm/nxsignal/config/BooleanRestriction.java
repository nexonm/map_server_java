/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.logging.NxLogger;

/**
 *
 * @author David.Vallejo
 */
public class BooleanRestriction implements ParamRestriction{

    private static final String TAG = "BooleanRestriction";
    
    @Override
    public boolean validateValue(Object value) {

        //NxLogger.verbose(TAG, "[validateValue] Boolean Restriction.");

        if(value == null){
            NxLogger.error(TAG, "[validateValue] Value is null");
            return false;
        }

        String strValue = value.toString();

        // Convert number version to Boolean.
        if(strValue.equalsIgnoreCase("0")) {
            strValue = Boolean.FALSE.toString();
        } else if (strValue.equalsIgnoreCase("1")) {
            strValue = Boolean.TRUE.toString();
        }
        
        if(!strValue.equalsIgnoreCase("false") && !strValue.equalsIgnoreCase("true")) {
            NxLogger.error(TAG, "[validateValue] Boolean value of object does not equal true or false.");
            return false;
        }
        
        return true;
    }

}
