/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author David.Vallejo
 */
public class NxAnalyticsConfiguration {

    private static final String TAG = "NxAnalyticsConfiguration";

    private Map<String, Object> config;
    private List<String> configErrors;
    private Map<String, Object> apiServerURLs;

    public NxAnalyticsConfiguration(JSONObject jsonObject) {

        config = new HashMap<String, Object>();
        configErrors = new ArrayList();

        try {

            apiServerURLs = NxUtils.convertJSONObjectToMap(jsonObject.getJSONObject(JsonKeys.ANALYTICS_API_SERVER_URL));
            config.put(JsonKeys.ANALYTICS_API_SERVER_URL, apiServerURLs);
            config.put(JsonKeys.LOGGING_MAX_LOG_SIZE, jsonObject.getInt(JsonKeys.LOGGING_MAX_LOG_SIZE));
            config.put(JsonKeys.LOGGING_REMOTE_LOGGING, jsonObject.getBoolean(JsonKeys.LOGGING_REMOTE_LOGGING));
            config.put(JsonKeys.ANALYTICS_PARTNER_ID, jsonObject.getString(JsonKeys.ANALYTICS_PARTNER_ID));
            config.put(JsonKeys.ANALYTICS_APP_ID, jsonObject.getString(JsonKeys.ANALYTICS_APP_ID));
            config.put(JsonKeys.COMMON_PARAMETER_SDK_VERSION_CONFIG, jsonObject.getString(JsonKeys.ANALYTICS_CONFIG_VERSION));
            config.put(JsonKeys.ANALYTICS_ERROR_EVENT_CHANNEL_ENABLED, jsonObject.getBoolean(JsonKeys.ANALYTICS_ERROR_EVENT_CHANNEL_ENABLED));

        } catch (JSONException ex) {
            configErrors.add(ex.getMessage());

            NxLogger.error(TAG, "[NxAnalyticsConfiguration] " + ex.getMessage());

        } catch (Exception ex) {
            configErrors.add(ex.getMessage());

            NxLogger.error(TAG, "[NxAnalyticsConfiguration] " + ex.getMessage());
        }

    }

    public boolean isValid(){
        return configErrors.isEmpty();
    }

    public Object getConfigValue(String key) {
        return config.get(key);
    }

    public String getApiServerUrl(String env) {
        return apiServerURLs.get(env).toString();
    }

    public String getAppId() {
        return (String) config.get(JsonKeys.ANALYTICS_APP_ID);
    }
}
