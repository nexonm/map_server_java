package com.nexonm.nxsignal.config;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by vincentfang on 2/22/16.
 */

public class NxAdapterConfiguration {

    private ArrayList configErrors;

    private String adapterIdentifier;
    private boolean enabled;
    private String loggingLevel;
    private Map<String, Object> configurationData;
    private Map<String, Map<String, NxAdapterColumnSpec>> supportedEvents;

    private static final String TAG = "NxAdapterConfiguration";

    public NxAdapterConfiguration(JSONObject adapterConfigJson) {

        configErrors = new ArrayList();

        configurationData = new HashMap<String, Object>();
        supportedEvents = new HashMap<String, Map<String, NxAdapterColumnSpec>>();

        try {
            adapterIdentifier = adapterConfigJson.getString(JsonKeys.ADAPTER_IDENTIFIER);
            enabled = adapterConfigJson.getBoolean(JsonKeys.ADAPTER_ENABLED);
            loggingLevel = adapterConfigJson.getString(JsonKeys.LOGGING_LEVEL);

            //NxLogger.verbose(TAG, "[NxAdapterConfiguration] adapter %s", adapterIdentifier);

            // Adapter specific configuration data
            if(adapterConfigJson.has(JsonKeys.ADAPTER_DATA)) {
                JSONObject dataJson = adapterConfigJson.getJSONObject(JsonKeys.ADAPTER_DATA);
                Iterator<String> adapterConfigItr = dataJson.keys();

                // Loop through all the data fields that are common to both ios and android
                while(adapterConfigItr.hasNext()) {
                    String dataName = adapterConfigItr.next();

                    //NxLogger.verbose(TAG, "[NxAdapterConfiguration] data field: %s", dataName);

                    // Skip over the ios adapter data
                    if(dataName.equalsIgnoreCase(JsonKeys.ADAPTER_IOS_DATA)) {
                        continue;
                    }

                    // If the data field is a common parameter for both adapters, add it to the configuration data
                    if(!dataName.equalsIgnoreCase(JsonKeys.ADAPTER_ANDROID_DATA)) {
                        Object value = dataJson.get(dataName);
                        configurationData.put(dataName, value);

                    // This is the adapter android specific data
                    } else {
                        JSONObject androidAdapterData = dataJson.getJSONObject((JsonKeys.ADAPTER_ANDROID_DATA));
                        Iterator<String> androidDataItr = androidAdapterData.keys();

                        // Iterate through all of the android specific data fields and put them in the configuration data
                        while(androidDataItr.hasNext()) {
                            String androidAdapterDataName = androidDataItr.next();

                            //NxLogger.verbose(TAG, "[NxAdapterConfiguration] android field: %s", androidAdapterDataName);

                            Object value = androidAdapterData.get(androidAdapterDataName);
                            configurationData.put(androidAdapterDataName, value);

                            // Locate the android package name in the adapter android data section and
                            // wake up the adapter
                            if(androidAdapterDataName.equalsIgnoreCase(JsonKeys.ADAPTER_DATA_ANDROID_PACKAGE) && value != null) {
                                String androidPackageName = (String) value;

                                //NxLogger.verbose(TAG, "[NxAdapterConfiguration] package name: %s", androidPackageName);

                                try {
                                    Class.forName(androidPackageName);
                                } catch (ClassNotFoundException e) {
                                    NxLogger.warn(TAG, "[NxAdapterConfiguration] Adapter Class Not Found" + e.getMessage());
                                }
                            }
                        }
                    }
                }
            }

            // Adapter's supported events and their parameters
            if(adapterConfigJson.has(JsonKeys.ADAPTER_EVENTS_SUPPORTED)) {
                JSONObject eventsJson = adapterConfigJson.getJSONObject(JsonKeys.ADAPTER_EVENTS_SUPPORTED);
                Iterator<String> eventTypeItr = eventsJson.keys();
                while(eventTypeItr.hasNext()) {
                    String eventType = eventTypeItr.next();
                    JSONObject parameterJson = eventsJson.getJSONObject(eventType);
                    JSONArray parametersJsonArray = parameterJson.getJSONArray(JsonKeys.ADAPTER_PARAMETERS);

                    // Loop through all the parameters for this event
                    HashMap<String, NxAdapterColumnSpec> eventParametersMap = new HashMap<String, NxAdapterColumnSpec>();
                    for(int parameterIndex = 0; parameterIndex < parametersJsonArray.length(); parameterIndex++) {
                        NxAdapterColumnSpec adapterColumnSpec =
                                new NxAdapterColumnSpec(parametersJsonArray.getJSONObject(parameterIndex));
                        eventParametersMap.put(adapterColumnSpec.getParameterName(), adapterColumnSpec);
                    }
                    supportedEvents.put(eventType, eventParametersMap);
                }
            }
        } catch (JSONException e) {
            configErrors.add(e.getMessage());

            NxLogger.error(TAG, "[NxAdapterConfiguration] " + e.getMessage());
        } catch (Exception e) {

            configErrors.add(e.getMessage());

            NxLogger.error(TAG, "[NxAdapterConfiguration] " + e.getMessage());
        }
    }

    public boolean isValid(){
        return configErrors.isEmpty();
    }

    public String getAdapterIdentifier() {
        return adapterIdentifier;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getLoggingLevel() {
        return loggingLevel;
    }

    public Map<String, Object> getConfigurationData() {
        return configurationData;
    }

    public int getNumberOfEventsInQueueFlushThreshold() {
        JSONObject eventProcessingTriggerJson = (JSONObject) configurationData.get(JsonKeys.ANALYTICS_EVENT_PROCESSING_TRIGGERS);
        int flushThreshold = 20;
        try {
            flushThreshold = eventProcessingTriggerJson.getInt(JsonKeys.ANALYTICS_NUMBER_OF_EVENTS_IN_QUEUE_FLUSH_THRESHOLD);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[getNumberOfEventsInQueueFlushThreshold] Error obtaining %s due to %s. Using default value %d",
                    JsonKeys.ANALYTICS_NUMBER_OF_EVENTS_IN_QUEUE_FLUSH_THRESHOLD, e.getMessage(), flushThreshold);
        }
        return flushThreshold;
    }

    public boolean isFlushTrigger(String eventType) {
        JSONObject eventProcessingTriggerJson = (JSONObject) configurationData.get(JsonKeys.ANALYTICS_EVENT_PROCESSING_TRIGGERS);

        JSONArray eventTriggersJson = null;
        try {
            eventTriggersJson = eventProcessingTriggerJson.getJSONArray(JsonKeys.ANALYTICS_TRIGGER_EVENTS);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[isFlushTrigger] Error obtaining %s from JSONObject.", JsonKeys.ANALYTICS_TRIGGER_EVENTS);
        }

        List triggers = NxUtils.convertJSONArrayToList(eventTriggersJson);
        Set<String> triggerEvents = new HashSet<String>(triggers);
        return triggerEvents.contains(eventType);
    }

    public int getMaxEventsPerBatchSend() {
        int maxEventsPerBatch = (Integer) configurationData.get(JsonKeys.ANALYTICS_MAX_EVENTS_PER_BATCH_SEND);
        return maxEventsPerBatch;
    }

    public int getTimeTillBatchSendingToServer() {
        JSONObject eventProcessingTriggerJson = (JSONObject) configurationData.get(JsonKeys.ANALYTICS_EVENT_PROCESSING_TRIGGERS);

        int timeTillBatchSendingSeconds = 60;
        try {
            timeTillBatchSendingSeconds = eventProcessingTriggerJson.getInt(JsonKeys.ANALYTICS_TIME_TILL_BATCH_SENDING_TO_SERVER);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[getTimeTillBatchSendingToServer] Error obtaining %s due to %s Using default value %d",
                    JsonKeys.ANALYTICS_TIME_TILL_BATCH_SENDING_TO_SERVER, e.getMessage(), timeTillBatchSendingSeconds);
        }

        return timeTillBatchSendingSeconds;
    }

    public int getSendCooldown() {
        JSONObject eventProcessingTriggerJson = (JSONObject) configurationData.get(JsonKeys.ANALYTICS_EVENT_PROCESSING_TRIGGERS);

        int sendCooldownSeconds = 1;
        try {
            sendCooldownSeconds = eventProcessingTriggerJson.getInt(JsonKeys.ANALYTICS_SEND_COOLDOWN);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[getSendCooldown] Error obtaining %s due to %s Using default value %d",
                    JsonKeys.ANALYTICS_SEND_COOLDOWN, e.getMessage(), sendCooldownSeconds);
        }

        return sendCooldownSeconds;
    }

    public int getEventStorageLimit() {
        int eventStorageLimit = (Integer) configurationData.get(JsonKeys.ANALYTICS_EVENT_STORAGE_LIMIT);
        return eventStorageLimit;
    }
}


