/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.config;


import com.nexonm.nxsignal.logging.NxLogger;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author David.Vallejo
 */
class StringRestriction implements ParamRestriction {

    private String pattern;
    private Set<String> validValues;
    private Integer length;
    private Integer minimumLength;

    private static final String TAG = "StringRestriction";

    StringRestriction(JSONObject jsonObject) throws JSONException, PatternSyntaxException {

        if (jsonObject.has(EventKeys.RESTRICTION_PATTERN)) {
            pattern = jsonObject.getString(EventKeys.RESTRICTION_PATTERN);
            verifyRegex(pattern);
        }
        
        if(jsonObject.has(EventKeys.RESTRICTION_LENGTH)){
            length = jsonObject.getInt(EventKeys.RESTRICTION_LENGTH);
        }

        if(jsonObject.has(EventKeys.RESTRICTION_LENGTH_MIN)) {
            minimumLength = jsonObject.getInt(EventKeys.RESTRICTION_LENGTH_MIN);
        }

        if (jsonObject.has(EventKeys.RESTRICTION_VALID_VALUES)) {
            JSONArray values = jsonObject.getJSONArray(EventKeys.RESTRICTION_VALID_VALUES);

            validValues = new HashSet<String>();

            for (int i = 0; i < values.length(); i++) {
                String currentValue = values.getString(i);
                validValues.add(currentValue);
            }
        }
    }

    private boolean verifyRegex(String regex) throws PatternSyntaxException {
        Pattern.compile(regex);

        return true;
    }

    @Override
    public boolean validateValue(Object value) {

        //NxLogger.verbose(TAG, "[validateValue] String Restriction.");

        if(value == null){
            NxLogger.error(TAG, "[validateValue] Value is null");
            return false;
        }

        if(value instanceof String){
            String strValue = (String)value;

            if(validValues != null){
                return validValues.contains(strValue);
            }

            if(pattern != null){
                if(!(strValue.matches(pattern))){
                    NxLogger.error(TAG, "[validateValue] Parameter value does not match regex pattern.");
                    return false;
                }
            }

            if(length != null){
                if(strValue.length() > length){
                    NxLogger.error(TAG, "[validateValue] Parameter value length " + strValue.length()
                            + " exceeds max length " + length);
                    return false;
                }
            }

            if(minimumLength != null) {
                if(strValue.length() < minimumLength) {
                    NxLogger.error(TAG, "[validateValue] Parameter value length " + strValue.length()
                            + " does not meet minimum length " + minimumLength);
                    return false;
                }
            }

        } else {
            NxLogger.error(TAG, "[validateValue] Parameter is not a String object.");
            return false;
        }
        return true;
    }

}
