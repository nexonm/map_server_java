/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.logging;

import java.util.logging.Logger;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public class NxLogger {

    //TODO: support exception logging of stack trace

    public enum LogLevel{

        /**
         *
         */
        VERBOSE("VERBOSE",1),
        INFO("INFO",2),
        WARN("WARN",3),
        ERROR("ERROR",4),
        FATAL("FATAL",5);

        private int intValue;
        private String strValue;

        private LogLevel(String neoStringValue, int neoIntValue){
            intValue = neoIntValue;
            strValue = neoStringValue;
        }

        @Override
        public String toString(){
            return strValue;
        }

        public int getIntValue(){
            return intValue;
        }

    }

    public static final String NEXON_TAG = "NexonM";
    private static LogLevel currentLogLevel = LogLevel.INFO;
    private static boolean isOutputLoggingToConsole;
    private static String environment = "";
    private final static Logger Log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    //private static NxSignalOptions options = new NxSignalOptions();



    private static void actuallyLogMessage(String prefix, LogLevel level ,String message, Object[] parameters) {

        if (currentLogLevel.getIntValue() > level.getIntValue()){
            return;
        }else if(!isOutputLoggingToConsole && level.getIntValue() < LogLevel.WARN.getIntValue() ) {
            return;
        }


        if(message == null){
            return;
        }


        //TODO: queue up log request so that they are written in antoher thread so as to not tie up
        //      the current thread
        //TODO: write to some buffer for log export some service or server



        String msgString = String.format(message,parameters);

        StringBuilder prefixBuilder = new StringBuilder();
        prefixBuilder.append(NEXON_TAG);
        prefixBuilder.append(" ");
        prefixBuilder.append(prefix);

        String theEnvironment = environment;

        prefixBuilder.append("["+theEnvironment+"]");
        prefix = prefixBuilder.toString();

        switch (level){
            case VERBOSE:
                Log.finest(prefix + " " + msgString);
                break;
            case INFO:
                Log.info(prefix  + " " + msgString);
                break;
            case WARN:
                Log.warning(prefix  + " " +  msgString);
                break;
            case ERROR:
                Log.severe(prefix  + " " + msgString);
                break;
            default:
            case FATAL:
                Log.severe(prefix  + " " + msgString);
                break;
        }
    }

    public static void setCurrentLogLevel(LogLevel level){
        currentLogLevel = level;
    }

    public static void setCurrentLogLevelFromString(String level) {
        if(level == null) {
            return;
        }

        if(level.equalsIgnoreCase("debug")) {
            currentLogLevel = LogLevel.VERBOSE;
        }
        else if(level.equalsIgnoreCase("info")) {
            currentLogLevel = LogLevel.INFO;
        }
        else if(level.equalsIgnoreCase("warn")) {
            currentLogLevel = LogLevel.WARN;
        }
        else if(level.equalsIgnoreCase("error")) {
            currentLogLevel = LogLevel.ERROR;
        }
        else if(level.equalsIgnoreCase("fatal")) {
            currentLogLevel = LogLevel.FATAL;
        }
        else {
            currentLogLevel = LogLevel.INFO;
        }
    }

    public static void setIsOutputLoggingToConsole(boolean isConsole) {
        isOutputLoggingToConsole = isConsole;
    }

    public static void setEnvironment(String pEnvironment) {

        if(pEnvironment != null) {
            environment = pEnvironment;
        } else {
            environment = "";
        }
    }

    /**
     * Send a VERBOSE log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged. as a format string
     * @param parameters the parameter arguments to the format message string
     */
    public static void verbose(String tag ,String msg,Object... parameters){
        actuallyLogMessage(tag,LogLevel.VERBOSE,msg,parameters);
    }


    /**
     * Send a INFO log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged. as a format string
     * @param parameters the parameter arguments to the format message string
     */
    public static void info(String tag ,String msg,Object... parameters){
        actuallyLogMessage(tag,LogLevel.INFO,msg,parameters);
    }

    /**
     * Send a WARN log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged. as a format string
     * @param parameters the parameter arguments to the format message string
     */
    public static void warn(String tag ,String msg,Object... parameters){
        actuallyLogMessage(tag,LogLevel.WARN,msg,parameters);
    }

    /**
     * Send a ERROR log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged. as a format string
     * @param parameters the parameter arguments to the format message string
     */
    public static void error(String tag ,String msg,Object... parameters){
        actuallyLogMessage(tag,LogLevel.ERROR,msg,parameters);
        String msgString = String.format(msg,parameters);
        NxErrorHandler.getInstance().sendError(msgString,0);
    }

    /**
     * Send a FATAL log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged. as a format string
     * @param parameters the parameter arguments to the format message string
     */
    public static void fatal(String tag ,String msg,Object... parameters){
        actuallyLogMessage(tag,LogLevel.FATAL,msg,parameters);
        NxErrorHandler.getInstance().sendError(msg,0);
    }



}
