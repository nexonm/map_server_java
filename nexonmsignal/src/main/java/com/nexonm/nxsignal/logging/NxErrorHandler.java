package com.nexonm.nxsignal.logging;

import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.config.NxAnalyticsConfiguration;
import com.nexonm.nxsignal.networking.NxErrorRequest;

import static com.nexonm.nxsignal.utils.NxUtils.isNullOrEmptyString;

/**
 * Created by jackyho on 7/29/16.
 */
public class NxErrorHandler {
    private String urlString;
    private String environment;
    private String appId;
    private boolean canSendEvent;
    private boolean errorEventChannelEnabled;
    private static NxErrorHandler sharedInstance = null;
    private Object _stateLock = new Object();

    private  NxErrorHandler() {
        canSendEvent = false;
    }

    public static synchronized NxErrorHandler getInstance(){
        if (sharedInstance == null) {
            sharedInstance = new NxErrorHandler();
        }
        return sharedInstance;
    }

    public boolean errorEventChannelEnabled() {
        return errorEventChannelEnabled;
    }

    public boolean canSendEvent() {
        return canSendEvent;
    }

    public void setup(String environment, NxAnalyticsConfiguration analyticsConfiguration) {

        if(analyticsConfiguration != null) {

            this.environment = environment;
            this.appId = analyticsConfiguration.getAppId();
            this.errorEventChannelEnabled = (Boolean) analyticsConfiguration.getConfigValue(JsonKeys.ANALYTICS_ERROR_EVENT_CHANNEL_ENABLED);
            this.urlString = analyticsConfiguration.getApiServerUrl(environment);

            //
            // If we have all the parameters we need, we can send the events.
            //
            if (errorEventChannelEnabled && !isNullOrEmptyString(this.urlString) && !isNullOrEmptyString(this.environment) && !isNullOrEmptyString(this.appId)) {
                synchronized (_stateLock) {
                    canSendEvent = true;
                }
            }
        }
    }

    public void sendError(String errorMessage, int errorCode) {
        synchronized (_stateLock) {
            if (canSendEvent) {
                NxErrorRequest errorRequest = new NxErrorRequest(errorMessage, errorCode, this.urlString, this.appId, this.environment);
                (new Thread(errorRequest)).start();
            }
        }
    }
}
