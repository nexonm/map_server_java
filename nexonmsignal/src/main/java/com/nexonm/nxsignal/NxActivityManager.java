/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal;

import com.nexonm.nxsignal.adapters.NxAdapter;
import com.nexonm.nxsignal.adapters.NxAdapterSignal;
import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.event.NxEventManager;
import com.nexonm.nxsignal.event.NxSDKGeneratedValues;
import com.nexonm.nxsignal.queue.DispatchQueue;
import com.nexonm.nxsignal.adapters.NxAdapterManager;
import com.nexonm.nxsignal.event.NxEvent;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

/**
 * Created by David Vallejo on 2/2/16.
 */
public class NxActivityManager {

    private static final String TAG = "NxActivityManager";
    //start queue for processing inputs
    private static final String DISPATCH_QUEUE_NAME = "io.Nexon.ActivityManager.ActivityQueue";

    //
    // Reference to the Adjust Adapter, if installed.
    //
    private NxAdapter adjustAdapter = null;

    private static NxActivityManager sharedInstance = null;

    public static synchronized NxActivityManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxActivityManager();
        }
        return sharedInstance;
    }

    private NxAdapterManager adapterManager;
    private NxAdapterSignal adapterSignal;

    private Map<String,Object> overwriteParamaters;

    private DispatchQueue internalQueue;

    private String apiKey = null;
    private NxSignalOptions signalOptions;

    private boolean isFirstResume;

    private NxActivityManager() {

        internalQueue = DispatchQueue.getQueueWithKey(DISPATCH_QUEUE_NAME, DispatchQueue.DrainStrategy.SERIAL);
        internalQueue.setPauseState(false);

        signalOptions = new NxSignalOptions();

        isFirstResume = true;

        overwriteParamaters = Collections.synchronizedMap(new HashMap<String, Object>());
    }

    Map<String, Object> getOverwriteParameters(){
        return new HashMap<String, Object>(this.overwriteParamaters);
    }

    void setNPSN(String npsn){
        NxSDKGeneratedValues.getInstance().storeNPSN(npsn);
    }

    void setFieldDefaultValues(Map<String, Object> pOverwriteParameters) {
        //
        // All objects are either Strings or primitives so this should
        // be sufficient for deep copying of a map. External developer would only change the mappings
        // of keys to values, not the values themselves.
        //
        Map<String, Object> overwriteParameters = new HashMap(pOverwriteParameters.size());

        Object[] keys = pOverwriteParameters.keySet().toArray();

        for(int i=0; i < keys.length; i++){
            String key = keys[i].toString();
            Object value = pOverwriteParameters.get(key);

            if(value != null) {
                if(key.equals(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID)) {
                    //
                    // If the parameters contain the developer_player_id, store it.
                    //
                    // NOTE : developers tend to set the developer_player_id as an int, but we want to make sure it
                    //        is always converted to a string internally.
                    //
                    String developerPlayerId = value.toString();
                    NxSDKGeneratedValues.getInstance().storeDeveloperPlayerId(developerPlayerId);

                    overwriteParameters.put(key, developerPlayerId);
                } else if(key.equals(JsonKeys.COMMON_PARAMETER_NPSN)) {
                    //
                    // Also, if the parameters contain the npsn, store it too.
                    //
                    // NOTE : developers tend to set the npsn as an int, but we want to make sure it
                    //        is always converted to a string internally.
                    //
                    String npsn = value.toString();
                    NxSDKGeneratedValues.getInstance().storeNPSN(npsn);

                    overwriteParameters.put(key, npsn);
                } else if(key.equals(JsonKeys.COMMON_PARAMETER_SIGNATURE)) {
                    //
                    // The server version of the SDK does not request the signature like the client does.
                    // We set it in the overwriteParameters, thus we need to forward it to the adapters.
                    //
                    String signature = value.toString();

                    this.adapterSignal.updateSignature(signature);

                    overwriteParameters.put(key, signature);
                } else {
                    overwriteParameters.put(key, value);
                }
            } else{
                NxLogger.warn(TAG, "[setFieldDefaultValues] the value for key %s is null and will be ignored", key);
            }
        }

        this.overwriteParamaters = Collections.synchronizedMap(overwriteParameters);
    }

    public void setup(String apiKey, NxSignalOptions signalOptions) {
        this.apiKey = apiKey;
        this.signalOptions = signalOptions;

        //
        // initialize adapter manager so we can register adapters
        //
        adapterManager = NxAdapterManager.getInstance();

        //
        // Nexon's adapter (internal adapter for processing events)
        //
        String signature = null;

        adapterSignal = new NxAdapterSignal(signature);
        adapterSignal.setup();

        adapterManager.addAdapter(adapterSignal);

        //
        // Adjust adapter - check if the library is found and the Adjust key has been set!
        //
        boolean adapterAdjustEnabled = false;
        String adapterAdjustKey = null;
        if(adapterAdjustEnabled) {
            if(adapterAdjustKey != null) {
                try{
                    Class c = Class.forName("com.nexonm.nxsignal.adapters.adjust.NxAdapterAdjust");

                    NxAdapter adapter = (NxAdapter)c.newInstance();
                    adapter.setup();

                    adapterManager.addAdapter(adapter);

                    //
                    // Save the reference to the Adjust Adapter
                    //
                    this.adjustAdapter = adapter;

                    NxLogger.verbose(TAG,"[setup] Adjust adapter is enabled, has been setup (with key %s) and is running...", adapterAdjustKey);

                }catch (Exception ex){
                    NxLogger.error(TAG,"[setup] Adjust library not found!");
                }
            } else {
                NxLogger.error(TAG,"[setup] Adjust key not found in manifest! Adapter won't be added...");
            }

        } else {
            NxLogger.info(TAG,"[setup] Adjust not enabled in manifest. Adapter won't be added...");
        }

        //
        // Start all the adapters
        //
        this.startAdapters();
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setOptions(NxSignalOptions pSignalOptions) {
        this.signalOptions.setOutputLoggingToConsole(pSignalOptions.isOutputLoggingToConsole());
        NxLogger.setIsOutputLoggingToConsole(pSignalOptions.isOutputLoggingToConsole());
    }

    public NxSignalOptions getOptions() {
        return signalOptions;
    }

    public String getEnvironment() { return signalOptions.getEnvironment(); }

    private void startAdapters() {
        adapterManager.startAdapters();
    }

    public void sendEvent(String pEventType, Map<String, Object> pEventSpecificParameters, Map<String, Object> pExtras) {

        final String eventType = pEventType;
        final Map<String, Object> overwriteParamaters = this.overwriteParamaters;
        final Map<String, Object> eventSpecificParameters = pEventSpecificParameters;
        final Map<String, Object> extras = pExtras;

        internalQueue.add(new Runnable() {
            @Override
            public void run() {
                //
                // First we create the event, with timestamp, sessionId and whatnot
                //
                NxEvent event = NxEventManager.getInstance().createEvent(eventType, overwriteParamaters, eventSpecificParameters, extras);

                NxLogger.verbose(TAG, "[sendEvent:run] Event of type %s was created. Forwarding to adapters...", event.getType());

                //
                // Send the event
                //
                adapterManager.sendEvent(event);
            }
        });
    }

    public boolean isSetup(){
        return apiKey != null;
    }

    public void onResumeUpdate(){

        boolean autoTrackLaunchEventsOn = signalOptions.isAutoTrackLaunchEvents();

        //
        // Autotracking is on and this is the main activity
        //
        if(autoTrackLaunchEventsOn == true) {

            // First time resumes are an app_open, otherwise they're regular resumes
            Map<String, Object> fields = new HashMap<String, Object>();
            if(isFirstResume == false) {
                fields.put("launch_type", NxLaunchType.RESUME.toString());
            } else {
                fields.put("launch_type", NxLaunchType.APP_OPEN.toString());
            }

            sendEvent(JsonKeys.EVENT_NAME_LAUNCH, fields, null);
        }

        isFirstResume = false;
    }

    /*
     *
     * Adjust Adapter specific methods
     *
     */
    public NxAdapter getAdjustAdapter() { return this.adjustAdapter; }

    public void sendAdjustEventWithCallbackValues(String token, Map values){
        try {
            if (this.adjustAdapter != null) {
                Method m = this.adjustAdapter.getClass().getMethod("sendAdjustEventWithCallbackValues", String.class, Map.class);
                m.invoke(this.adjustAdapter, token, values);
            }
        }catch (Exception ex){
            NxLogger.error(TAG,"[sendAdjustEventWithCallbackValues] Failed to send Adjust event");
        }
    }

}
