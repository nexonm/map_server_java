/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.utils;

import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.logging.NxLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author David.Vallejo
 */
public class NxUtils {

    private static final String TAG = "NxUtils";

    public static String getSdkVersion() {
        return JsonKeys.SDK_VERSION;
    }

    public static Map<String,Object> convertJSONObjectToMap(JSONObject json){
        Map<String,Object> result = new HashMap<String,Object>();
        if(json != null) {
            for (Iterator<String> keyIterator = json.keys(); keyIterator.hasNext();) {
                try {
                    String key = keyIterator.next();
                    Object value = json.get(key);
                    result.put(key, value);
                } catch (JSONException ex) {
                    NxLogger.error(TAG, "[convertJSONObjectToMap] Error %s ", ex.getMessage());
                    //Logger.getLogger(NxUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }

    public static String[] convertJSONArrayToArray(JSONArray json) {
        return (String[])convertJSONArrayToList(json).toArray();
    }

    // Always returns a non null list
    public static List<String> convertJSONArrayToList(JSONArray json) {
        ArrayList<String> result = new ArrayList<String>();
        for(int index = 0; json != null && index < json.length(); index++) {
            try {
                result.add((String) json.getString(index));
            } catch (JSONException ex) {
                Logger.getLogger(NxUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    public static String urlEncodeUTF8(Map<String,String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            try {
                Object rawKey = entry.getKey();
                Object rawValue = entry.getValue();
                if(rawKey != null && rawValue != null) {
                    String key = URLEncoder.encode(rawKey.toString(), "UTF-8");
                    String value = URLEncoder.encode(rawValue.toString(), "UTF-8");

                    sb.append(String.format("%s=%s", key, value));
                }else{
                    NxLogger.error(TAG, "[urlEncodeUTF8] Error null key or value  key=%s ", rawKey);
                }
            } catch (UnsupportedEncodingException ex) {
                NxLogger.error(TAG, "[urlEncodeUTF8] Error %s ", ex.getMessage());
            }catch (Exception ex){
                NxLogger.error(TAG, "[urlEncodeUTF8] Error %s ", ex.getMessage());
            }
        }
        return sb.toString();
    }

    public static Map clearNulls(Map originalMap, List keysThatAreNullValue){
        if(originalMap == null){
            return null;
        }

        Map result = new HashMap(originalMap.size());

        Object[] keys = originalMap.keySet().toArray();

        for(int i=0;i<keys.length;i++){
            Object value = originalMap.get(keys[i]);
            if(value != null) {
                result.put(keys[i],value);
            }else{
                if(keysThatAreNullValue != null){
                    keysThatAreNullValue.add(keys[i]);
                }
            }
        }

        return result;
    }


    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public static boolean isUnity(){
        boolean result = false;

        final String UNITY_COMMUNICATOR_CLASS_NAME = "com.nexonm.bridge.UnityCommunicator";

        try{
            Class.forName(UNITY_COMMUNICATOR_CLASS_NAME);
            result = true;

            NxLogger.verbose(TAG, "[isUnity] %s class found. This is an Unity Android app.",
                    UNITY_COMMUNICATOR_CLASS_NAME);
        }catch(ClassNotFoundException e){
            //Class not found so false
            NxLogger.verbose(TAG, "[isUnity] %s class not found. Assuming this is a native Android app now. %s",
                    UNITY_COMMUNICATOR_CLASS_NAME, e.getMessage());
        }

        return result;
    }


    public static String getStackTraceAsString(Throwable ex){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        return sw.toString();
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isNullOrEmptyString(String str) {
        return (str == null || str.length() <= 0);
    }

    public static String createPartnerIdForAppId(String appId) {

        if(isNullOrEmptyString(appId))
        {
            // But hey, this should never happen, come on!
            return "map";
        }

        String partnerId = null;

        if (appId.equalsIgnoreCase("dwu")) {
            partnerId = "xpec";
        } else if (appId.equalsIgnoreCase("returners")) {
            partnerId = "ds";
        } else if (appId.equalsIgnoreCase("woh")) {
            partnerId = "fuero";
        } else if (appId.equalsIgnoreCase("projectmr")) {
            partnerId = "est";
        } else if (appId.equalsIgnoreCase("tfa")) {
            partnerId = "particlecity";
        } else if (appId.equalsIgnoreCase("durango")) {
            partnerId = "nexon";
        } else if (appId.equalsIgnoreCase("maplem")) {
            partnerId = "nsc";
        } else if (appId.equalsIgnoreCase("boh")) {
            partnerId = "popcorn";
        } else if (appId.equalsIgnoreCase("battlejack")) {
            partnerId = "grandcru";
        } else {
            partnerId = "map";
        }

        return partnerId;
    }
}
