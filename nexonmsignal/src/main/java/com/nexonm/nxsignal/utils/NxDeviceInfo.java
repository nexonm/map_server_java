/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.utils;

import com.nexonm.nxsignal.NxSignalOptions;

/**
 * Created by David.Vallejo on 2/3/16.
 */
public class NxDeviceInfo {


    private static NxDeviceInfo sharedInstance = null;

    public static synchronized NxDeviceInfo getInstance(){
        if (sharedInstance == null){
            sharedInstance = new NxDeviceInfo();
        }
        return sharedInstance;
    }

    private NxDeviceInfo() {
    }

    public String getDeviceName() {
        return "device-name";
    }

    public String getDeviceMake() {
        return "device-make";
    }

    public String getDeviceModel() {
        return "device-model";
    }

    public String getOSPlatform() {
        return "os-platform";
    }

    public String getOSName() {
        return "os-name";
    }
}
