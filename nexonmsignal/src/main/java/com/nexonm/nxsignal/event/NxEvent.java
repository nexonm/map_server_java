/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.event;

import com.nexonm.nxsignal.utils.NxUtils;

import java.util.HashMap;
import org.json.JSONObject;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

/**
 *
 * @author David.Vallejo
 */
public class NxEvent {
    private static final String TAG = "NxEvent";

    private static final String KEY_EVENT_NAME = "event_name";
    private static final String KEY_EVENT_TYPE = "event_type";
    private static final String KEY_PRIORITY = "priority";
    private static final String KEY_PARAMETERS = "parameters";
    private static final String KEY_EXTRAS = "extras";

    private final String type;
    private final Map<String, Object> parameters;
    private final int priority;
    private final Map<String, Object> extras; // Non toString objects. Used for special adapters.

    public NxEvent(String eventType, int priority, Map<String, Object> parameters, Map<String, Object> extras) {
        this.type = eventType;
        this.priority = priority;
        this.parameters = parameters;

        if(extras == null) {
            extras = new HashMap<String, Object>();
        }
        this.extras = extras;
    }

    public NxEvent(JSONObject spec) throws JSONException {

        if(spec.has(KEY_EVENT_NAME)) {
            //
            // New version of events has 'event_name'
            //
            this.type = spec.getString(KEY_EVENT_NAME);
        } else {
            //
            // Required to load events from storage files, where 'event_name' was still 'event_type'
            //
            this.type = spec.getString(KEY_EVENT_TYPE);
        }

        this.priority = spec.getInt(KEY_PRIORITY);
        this.parameters = NxUtils.convertJSONObjectToMap(spec.getJSONObject(KEY_PARAMETERS));

        if(spec.has(KEY_EXTRAS)) {
            this.extras = NxUtils.convertJSONObjectToMap(spec.getJSONObject(KEY_EXTRAS));
        } else {
            this.extras = new HashMap<String, Object>();
        }
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public String getType() {
        return type;
    }

    public String getBody() {
        JSONObject jsonParams = new JSONObject(parameters);
        return jsonParams.toString();
    }

    public int getPriority() {
        return priority;
    }

    public Map<String, Object> getExtras() {
        return extras;
    }

    @Override
    public String toString() {
        String result = null;
        try {
            JSONObject jsonObj = new JSONObject();

            JSONObject jsonParams = new JSONObject(parameters);
            jsonObj.put(KEY_PARAMETERS, jsonParams);

            jsonObj.put(KEY_EVENT_NAME, this.type);
            jsonObj.put(KEY_PRIORITY, this.priority);
            
            result = jsonObj.toString();
        } catch (JSONException ex) {
            Logger.getLogger(NxEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public JSONObject toEventJSON() {
        Map<String, Object> hashMap = new HashMap<String, Object>();

        for (String parameterName : parameters.keySet()) {
            hashMap.put(parameterName, parameters.get(parameterName));
        }

        return new JSONObject(hashMap);
    }

}
