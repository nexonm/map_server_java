package com.nexonm.nxsignal.event;

/**
 * Created by giorgio.tino on 6/1/17.
 */

public final class NxPlatformDeviceIdInfo {
    private String id;
    private String idType;

    public NxPlatformDeviceIdInfo(String deviceId, String deviceIdType) {
        id = deviceId;
        idType = deviceIdType;
    }

    public String getId() { return id; }
    public String getIdType() { return idType; }
}
