package com.nexonm.nxsignal.event;

import com.nexonm.nxsignal.NxActivityManager;
import com.nexonm.nxsignal.NxLaunchType;
import com.nexonm.nxsignal.NxSignal;
import com.nexonm.nxsignal.NxSignalOptions;
import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.config.NxAnalyticsConfiguration;
import com.nexonm.nxsignal.config.NxConfiguration;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.networking.NxHttpService;
import com.nexonm.nxsignal.networking.NxHttpTask;
import com.nexonm.nxsignal.networking.NxHttpTaskHandler;
import com.nexonm.nxsignal.queue.DispatchQueue;
import com.nexonm.nxsignal.storage.NxPersistentStorage;
import com.nexonm.nxsignal.storage.NxStorage;
import com.nexonm.nxsignal.utils.NxDeviceInfo;
import com.nexonm.nxsignal.config.NxConfigurationManager;
import com.nexonm.nxsignal.utils.NxUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.nexonm.nxsignal.utils.NxUtils.isNullOrEmptyString;

/**
 * Created by David.Vallejo on 2/2/16.
 *
 * NOTE : this class MUST be called NxSDKGeneratedValues as Toy loads it via reflection!
 *
 */
public class NxSDKGeneratedValues {
    
    private static final String TAG = "NxSDKGeneratedValues";

    private static final String DISPATCH_QUEUE_NAME = "io.Nx.NxSDKGeneratedValues";

    //
    // Legacy file names, from a time when properties were stored in the filesystem... Now the same
    // properties are stored in the Shared Properties, but we still need to be backward compatible
    // to properly load the properties from the files...
    //
    private static final String NX_SDK_GENERATED_VALUES_STORAGE_FILE = "NxSDKGeneratedValuesStorageFile";
    private static final String NX_LAST_EVENT_SENT_TIMESTAMP_STORAGE_FILE = "NxLastEventSentTimestampStorageFile";
    private static final String NX_SESSION_ID_STORAGE_FILE = "NxSessionIdStorageFile";


    private DispatchQueue internalQueue;
    private Map<String, Object> parameters;

    private boolean isReady = false;

    private String apiKey;
    private NxSignalOptions signalOptions;


    private Object lastEventSentTimestampLock = new Object();
    private double lastEventSentTimestamp = 0;

    private Object sessionIdLock = new Object();
    private double sessionId = 0;

    private static NxSDKGeneratedValues sharedInstance = null;

    public static synchronized NxSDKGeneratedValues getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxSDKGeneratedValues();
        }
        return sharedInstance;
    }

    private NxSDKGeneratedValues() {
        internalQueue = DispatchQueue.getQueueWithKey(DISPATCH_QUEUE_NAME, DispatchQueue.DrainStrategy.SERIAL);
        internalQueue.setPauseState(false);

        //
        // Legacy code required for backward compatibility - if the files are present, we do need to
        // read them, get the values from there, store them in the persistent storage and then remove
        // the files. This operation will be performed one time only, so we make it synchronous because
        // it doesn't really matter that much. And it is way easier this way.
        //
        recoverStorage();

        //
        // Retrieve the stored values for session id and last event sent timestamp
        //
        if(NxPersistentStorage.getInstance().has(JsonKeys.COMMON_PARAMETER_SESSION_ID)) {
            this.setSessionId(Double.parseDouble(NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_SESSION_ID)));

            NxLogger.verbose(TAG,"[NxSDKGeneratedValues] Session Id found in persistent storage: " + getSessionId());
        }
        if(NxPersistentStorage.getInstance().has(JsonKeys.COMMON_PARAMETER_LAST_EVENT_SENT_TIMESTAMP)) {
            this.setLastEventSentTimestamp(Double.parseDouble(NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_LAST_EVENT_SENT_TIMESTAMP)));

            NxLogger.verbose(TAG,"[NxSDKGeneratedValues] Last Event Sent Timestamp found in persistent storage: " + getLastEventSentTimestamp());
        }
    }

    private void recoverStorage() {
        recoverStorageFromJsonFile(NX_SDK_GENERATED_VALUES_STORAGE_FILE);
        recoverStorageFromTextFile(NX_LAST_EVENT_SENT_TIMESTAMP_STORAGE_FILE, JsonKeys.COMMON_PARAMETER_LAST_EVENT_SENT_TIMESTAMP);
        recoverStorageFromTextFile(NX_SESSION_ID_STORAGE_FILE, JsonKeys.COMMON_PARAMETER_SESSION_ID);
    }

    private void recoverStorageFromJsonFile(String path) {
        if(NxStorage.getInstance().fileExists(path)) {
            NxLogger.info(TAG,"[recoverStorageFromJsonFile] Legacy %s still exists. Loading the data...",path);

            String content = NxStorage.getInstance().loadStringFromFile(path,"UTF-8");

            try {
                JSONObject jsonObject = new JSONObject(content);

                //
                // Read all the values found in the legacy storage file
                //
                for(int i = 0; i<jsonObject.names().length(); i++){
                    String key = jsonObject.names().getString(i);
                    String value = jsonObject.getString(key);

                    //
                    // Store the value found in the file into the Persistent Storage
                    //
                    NxPersistentStorage.getInstance().putString(key, value);
                }

            } catch (JSONException ex) {
                NxLogger.error(TAG, "[recoverStorageFromJsonFile] Cannot parse JSON content from %s legacy storage file.", path);
            } finally {
                //
                // In any case, we get rid of the file, assuming that if its content was not ok once,
                // it will never be ok ever.
                //
                NxStorage.getInstance().deleteFile(path);
            }

        } else {
            NxLogger.verbose(TAG,"[recoverStorageFromJsonFile] Legacy %s does not exist. Skipping...", path);
        }
    }

    private void recoverStorageFromTextFile(String path, String parameterName) {
        if(NxStorage.getInstance().fileExists(path)) {
            NxLogger.info(TAG,"[recoverStorageFromJsonFile] Legacy %s still exists. Loading the data...",path);

            String content = NxStorage.getInstance().loadStringFromFile(path,"UTF-8");

            //
            // Store the value found in the file into the Persistent Storage, with the given parameterName as key
            //
            NxPersistentStorage.getInstance().putString(parameterName, content);

            //
            // In any case, we get rid of the file.
            //
            NxStorage.getInstance().deleteFile(path);

        } else {
            NxLogger.verbose(TAG,"[recoverStorageFromJsonFile] Legacy %s does not exist. Skipping...", path);
        }
    }

    public void setup(String apiKey, NxSignalOptions signalOptions) {
        this.apiKey = apiKey;
        this.signalOptions = signalOptions;

        //
        // Required to be run in a thread different than the main one
        //
        loadParameters();
    }

    private void loadParameters() {
        NxLogger.info(TAG, "[loadParameters] Loading parameters...");

        parameters = new HashMap<String, Object>();

        //
        // Retrieve (or generate) the Nexon Device ID (a.k.a. uuid)
        //
        String nexonDeviceId = getOrCreateNexonDeviceId();

        //
        // Add it to the new generate values hashmap
        //
        parameters.put(JsonKeys.COMMON_PARAMETER_NEXON_DEVICE_ID, nexonDeviceId);

        //
        // Developer Player Id (if present)
        //
        if (NxPersistentStorage.getInstance().has(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID)) {
            parameters.put(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID,
                    NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID));
        }

        //
        // NPSN (if present)
        //
        if (NxPersistentStorage.getInstance().has(JsonKeys.COMMON_PARAMETER_NPSN)) {
            parameters.put(JsonKeys.COMMON_PARAMETER_NPSN,
                    NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_NPSN));
        }

        NxConfiguration configuration = NxConfigurationManager.getInstance().getConfiguration();
        NxAnalyticsConfiguration clientAnalyticsConfig = configuration.getAnalyticsConfiguration();

        //
        // Start filling out all the parameters we can fill
        //
        parameters.put(JsonKeys.ANALYTICS_PARTNER_ID, clientAnalyticsConfig.getConfigValue(JsonKeys.ANALYTICS_PARTNER_ID));
        parameters.put(JsonKeys.COMMON_PARAMETER_APP_LOCALE, Locale.getDefault().toString());
        parameters.put(JsonKeys.SPECIAL_PARAMETER_ENV, signalOptions.getEnvironment());
        String deviceType = "desktop";

        //
        // Parmeters from NxDeviceInfo
        //
        parameters.put(JsonKeys.COMMON_PARAMETER_DEVICE_TYPE, deviceType);
        parameters.put(JsonKeys.COMMON_PARAMETER_DEVICE_MAKE, NxDeviceInfo.getInstance().getDeviceMake());
        parameters.put(JsonKeys.COMMON_PARAMETER_DEVICE_MODEL, NxDeviceInfo.getInstance().getDeviceModel());
        parameters.put(JsonKeys.COMMON_PARAMETER_DEVICE_NAME, ""); // YES, we do need the parameter (as it is marked as required for server validation), but we don't want to send the value, so we keep it empty.
        parameters.put(JsonKeys.COMMON_PARAMETER_OS_PLATFORM, NxDeviceInfo.getInstance().getOSPlatform());
        parameters.put(JsonKeys.COMMON_PARAMETER_OS_NAME, NxDeviceInfo.getInstance().getOSName());
        parameters.put(JsonKeys.COMMON_PARAMETER_SDK_VERSION, NxUtils.getSdkVersion());

        if (signalOptions.getService_id() != null) {
            parameters.put(JsonKeys.COMMON_PARAMETER_TOY_SERVICE_ID, signalOptions.getService_id());
        }

        isReady = true;

        NxLogger.info(TAG, "[loadParameters] Parameters loaded.");

        NxLogger.info(TAG, "[loadParameters] sdk_version :%s", NxUtils.getSdkVersion());
        NxLogger.info(TAG, "[loadParameters] nexon_device_id : %s", (String) parameters.get(JsonKeys.COMMON_PARAMETER_NEXON_DEVICE_ID));
    }

    public boolean isReady() {
        return isReady;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    //
    // Note: must be called from separate thread or will raise an exception!
    //
    public synchronized NxPlatformDeviceIdInfo getOrCreatePlatformDeviceIdInfo() {
        String platformDeviceId = null;
        String platformDeviceIdType = "custom";

        try {

            if(platformDeviceId == null || platformDeviceId.isEmpty()) {

                NxLogger.warn(TAG, "[getOrCreatePlatformDeviceIdInfo] Unable to retrieve platform ID, using custom ID as platform device ID.");

                // Retrieve previous custom ID (if it exists) from the persistent storage
                platformDeviceId = NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_PLATFORM_DEVICE_ID);

                // generate a new one
                if(platformDeviceId == null || platformDeviceId.isEmpty()) {
                    platformDeviceId = NxUtils.generateUUID();
                    platformDeviceIdType = "custom";

                    //
                    // Also store the custom platform device id into the persistent storage, so at least we will
                    // get the same one next time the user runs the app
                    //
                    NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_PLATFORM_DEVICE_ID, platformDeviceId);
                }
            }
            else {
                platformDeviceIdType = "aaid";
            }

            NxLogger.info(TAG, "[getOrCreatePlatformDeviceIdInfo] Platform Device ID: %s, Type: %s",platformDeviceId,platformDeviceIdType);

        } catch(Exception e){
            NxLogger.error(TAG, "[getOrCreatePlatformDeviceIdInfo] Error, Unexpected error exception: %s.", e.getMessage());
        }

        return new NxPlatformDeviceIdInfo(platformDeviceId,platformDeviceIdType);
    }

    public synchronized String getOrCreateNexonDeviceId() {
        String nexonDeviceId = null;

        nexonDeviceId = NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_NEXON_DEVICE_ID);

        if (isNullOrEmptyString(nexonDeviceId)) {

            NxLogger.info(TAG, "[getOrCreateNexonDeviceId] nexon_device_id not found in persistent storage. Generating a new one...");

            //
            // nexon_device_id does not already exist, generate a new one!
            //
            nexonDeviceId = NxUtils.generateUUID();

            NxLogger.info(TAG, "[getOrCreateNexonDeviceId] New nexon_device_id generated: %s", nexonDeviceId);

            //
            // ...and save it to the persistent storage
            //
            NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_NEXON_DEVICE_ID, nexonDeviceId);

            //
            // If Adjust bridging is set in the metadata, then we can now issue a bridging event
            // to bind this nexon_device_id with Adjust
            //
            String at = null;
            if (at != null) {
                HashMap values = new HashMap();
                values.put(JsonKeys.COMMON_PARAMETER_NEXON_DEVICE_ID, nexonDeviceId);

                NxSignal.sendAdjustEventWithCallbackValues(at, values);
            } else {
                NxLogger.warn(TAG, "[getOrCreateNexonDeviceId] %s not found in metadata. Won't send bridging event...",JsonKeys.ADAPTER_ADJUST_EVENT_TOKEN_NAME_BRIDGING);
            }

        } else {
            NxLogger.verbose(TAG, "[getOrCreateNexonDeviceId] nexon_device_id found in persistent storage: %s", nexonDeviceId);
        }

        return nexonDeviceId;
    }

    //
    // NOTE : this method MUST exist, as Toy will try to call it via reflection. This is the same reason why this class
    //        MUST be called NxCommonFields
    //
    public String getUUID() {
        return getOrCreateNexonDeviceId();
    }

    public double getSessionId() {
        double result = 0;
        synchronized (sessionIdLock) {
            result = sessionId;
        }
        return result;
    }

    private void setSessionId(double neoSessionId){
        synchronized (sessionIdLock){
            sessionId = neoSessionId;
        }
    }

    public void storeSessionId(double givenSessionId) {
        this.setSessionId(givenSessionId);

        NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_SESSION_ID, this.getSessionId()+"");
    }


    public double getLastEventSentTimestamp() {
        double result = 0;
        synchronized (lastEventSentTimestampLock) {
            result = lastEventSentTimestamp;
        }
        return result;
    }

    private void setLastEventSentTimestamp(double neoLastEventSentTimestamp){
        synchronized (lastEventSentTimestampLock) {
            lastEventSentTimestamp = neoLastEventSentTimestamp;
        }
    }

    public void storeLastEventSentTimestamp(double givenLastEventSentTimestamp) {
        this.setLastEventSentTimestamp(givenLastEventSentTimestamp);

        NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_LAST_EVENT_SENT_TIMESTAMP,this.getLastEventSentTimestamp()+"");
    }

    public void storeDeveloperPlayerId(String pDeveloperPlayerId) {

        if(pDeveloperPlayerId != null) {
            //
            // Store the developer player id to the persistent storage
            //
            NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID, pDeveloperPlayerId);

            //
            // Save it to current parameters too
            //
            parameters.put(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID, pDeveloperPlayerId);

            //
            // Report the event (First time resumes are an app_open, otherwise they're regular resumes)
            //
            Map<String, Object> fields = new HashMap<String, Object>();
            fields.put(JsonKeys.EVENT_LAUNCH_TYPE, NxLaunchType.LOGIN.toString());

            NxSignal.reportCustomEvent(JsonKeys.EVENT_NAME_LAUNCH, fields, null);
        }
    }

    public String getDeveloperPlayerId() {
        return NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_DEVELOPER_PLAYER_ID);
    }

    public void storeNPSN(String pNPSN) {

        if(pNPSN != null) {
            //
            // Store the NPSN to the persistent storage
            //
            NxPersistentStorage.getInstance().putString(JsonKeys.COMMON_PARAMETER_NPSN, pNPSN);

            //
            // Save it to current parameters too
            //
            parameters.put(JsonKeys.COMMON_PARAMETER_NPSN, pNPSN);

            //
            // Report the event (First time resumes are an app_open, otherwise they're regular resumes)
            //
            Map<String, Object> fields = new HashMap<String, Object>();
            fields.put(JsonKeys.EVENT_LAUNCH_TYPE, NxLaunchType.LOGIN.toString());

            NxSignal.reportCustomEvent(JsonKeys.EVENT_NAME_LAUNCH, fields, null);
        }
    }

    public String getNPSN() {
        return NxPersistentStorage.getInstance().getString(JsonKeys.COMMON_PARAMETER_NPSN);
    }
}
