package com.nexonm.nxsignal.event;

import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by giorgiotino on 8/8/17.
 */

public class NxEventManager {

    private static final String TAG = "NxEventManager";
    private static final long IDLE_TIME_FOR_SESSION_ID_CHANGE_MINUTES = 5 * 60;
    private static final int EVENT_PRIORITY_DEFAULT = 1;

    private double lastTimeStamp = 0;

    //
    // Prevent public constructor usage
    //
    private NxEventManager() {}

    private static NxEventManager sharedInstance = null;

    public static synchronized NxEventManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxEventManager();
        }
        return sharedInstance;
    }

    public void setup() { }

    public NxEvent createEvent(String type, Map<String, Object> overwriteParamaters, Map<String, Object> eventSpecificParameters, Map<String, Object> extras) {
        //
        // retrieve the current session Id and the last event sent timestamp here
        //
        double currentSessionId = NxSDKGeneratedValues.getInstance().getSessionId();
        double lastEventSentTimestamp = NxSDKGeneratedValues.getInstance().getLastEventSentTimestamp();

        double currentTimestamp = getTimestamp();

        if((currentTimestamp - lastEventSentTimestamp) > IDLE_TIME_FOR_SESSION_ID_CHANGE_MINUTES) {
            NxLogger.verbose(TAG, "[createEvent] 5 minutes have passed, updating session id.");
            currentSessionId = currentTimestamp;
            NxSDKGeneratedValues.getInstance().storeSessionId(currentSessionId);
        }

        Map<String, Object> parameters = new HashMap<String, Object>();

        //
        // Inject the DEFAULT generated values first
        //
        parameters.putAll(NxSDKGeneratedValues.getInstance().getParameters());

        //
        // Inject the DYNAMIC overwriteParamaters
        //
        parameters.put(JsonKeys.COMMON_PARAMETER_EVENT_TYPE, type);
        parameters.put(JsonKeys.COMMON_PARAMETER_SESSION_ID, currentSessionId);
        parameters.put(JsonKeys.COMMON_PARAMETER_APPLICATION_TIMESTAMP, currentTimestamp);
        boolean offline = !this.isOnline();
        parameters.put(JsonKeys.COMMON_PARAMETER_BATCH_OFFLINE, offline);

        //
        // Layer the OVERWRITE default values next
        //
        parameters.putAll(overwriteParamaters);

        //
        // Layer the event SPECIFIC parameters next (clean them first!)
        //
        for (Map.Entry<String, Object> pair : eventSpecificParameters.entrySet()) {
            if(pair.getValue() == null) {
                //
                // Do not add a null parameter!
                //
                NxLogger.warn(TAG, "[createEvent] The value for the event specific parameter %s is null, and the parameter will be ignored.", pair.getKey());
            } else {
                //
                // Add the parameter, since it is not null...
                //
                parameters.put(pair.getKey(), pair.getValue());

                NxLogger.verbose(TAG, "[createEvent] Event specific parameter %s added. Value is %s.", pair.getKey(), pair.getValue().toString());
            }
        }

        //
        // Process event asynchronously, without client validation!
        //
        int priority = EVENT_PRIORITY_DEFAULT;

        //
        // Add event_id, a unique UUID to identify the event and help the server discard duplicates.
        //
        // NOTE : developers cannot override this value!
        //
        String eventId = NxUtils.generateUUID();
        parameters.put(JsonKeys.COMMON_PARAMETER_EVENT_ID, eventId);

        //
        // Create the event
        //
        final NxEvent event = new NxEvent(type, priority, parameters, extras);

        //
        // Save last event timestamp
        //
        lastEventSentTimestamp = currentTimestamp;
        NxSDKGeneratedValues.getInstance().storeLastEventSentTimestamp(lastEventSentTimestamp);

        return event;
    }

    private double getTimestamp(){
        double time = System.currentTimeMillis() ;
        if(time == lastTimeStamp){
            time ++;
        }
        lastTimeStamp = time;
        return time / 1000;
    }

    private boolean isOnline() {
        return true;
    }
}
