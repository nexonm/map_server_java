/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal;

import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.event.NxEventManager;
import com.nexonm.nxsignal.event.NxSDKGeneratedValues;
import com.nexonm.nxsignal.logging.NxErrorHandler;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.storage.NxDatabase;
import com.nexonm.nxsignal.storage.NxPersistentStorage;
import com.nexonm.nxsignal.config.NxConfigurationManager;
import com.nexonm.nxsignal.storage.NxStorage;
import com.nexonm.nxsignal.utils.NxUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONException;

import static com.nexonm.nxsignal.utils.NxUtils.isNullOrEmptyString;

/**
 * The NxSignal is the class for interfacing with the analytics
 */
public class NxSignal {

    private static final String TAG = "NxSignal";
    public static void start(String apiKey, NxSignalOptions signalOptions) {

        //
        // Options cannot be null
        //
        if(signalOptions == null) {
            NxLogger.fatal(TAG,"[start] Signal Options parameter cannot be null!");
            throw new IllegalArgumentException("Signal Options parameter cannot be null!");
        }

        NxLogger.setCurrentLogLevelFromString(signalOptions.getLogLevel());
        NxLogger.setIsOutputLoggingToConsole(signalOptions.isOutputLoggingToConsole());
        NxLogger.setEnvironment(signalOptions.getEnvironment());

        //
        // Setup the Database storage
        //
        NxDatabase.getInstance().setup();

        //
        // Setup the persistent storage
        //
        NxPersistentStorage.getInstance().setup();

        //
        // Setup the storage
        //
        NxStorage.getInstance().setup();

        //
        // Call NxConfigurationManager here to startup the setup process
        //
        NxConfigurationManager.getInstance().setup(signalOptions);

        if(NxConfigurationManager.getInstance().isReady()) {
            //
            // Api key - if the one pase in is null, try to generate it with the gameId taken from the
            // config or the options.
            //
            if(apiKey == null) {
                //
                // If api key is null, then game_id in options MUST be set in order to generate a key
                // on the fly.
                //
                String gameId = NxConfigurationManager.getInstance().getConfiguration().getAnalyticsConfiguration().getAppId();

                if( isNullOrEmptyString(gameId) ) {
                    NxLogger.fatal(TAG,"[start] API Key is null and a default one could not be generated (gameId in option and config is null or empty)!");
                    throw new IllegalArgumentException("API Key is null and a default one could not be generated (gameId in option and config is null or empty)!");
                } else {
                    apiKey = NxUtils.md5("toy." + gameId).toLowerCase();
                }
            }

            //
            // First time setup, now that NxConfigurationManager has finished setup, telling  NxSDKGeneratedValues boot up.
            //
            NxSDKGeneratedValues.getInstance().setup(apiKey, signalOptions);

            //
            // NxEventManager is the manager used to create events. It manages timestamps and sessionIds.
            //
            NxEventManager.getInstance().setup();

            //
            // Setup the error handler
            // Requires analytics config and activity manager to be loaded, up and running!
            //
            NxErrorHandler.getInstance().setup(signalOptions.getEnvironment(), NxConfigurationManager.getInstance().getConfiguration().getAnalyticsConfiguration());

            //
            // The NxActivityManager must be called here in order to ensure the NxActivityManager will register
            // with the Activity Lifecycle as soon as NxSignal.start() is called.
            //
            NxActivityManager.getInstance().setup(apiKey, signalOptions);
        } else {
            //
            // The config is not ready/valid: this can only be because a gameId has not been found anywhere!
            //
            NxLogger.fatal(TAG, "[start] Configuration Manager is not ready, configuration was not loaded! This usually means that a valid gameId was not found neither in the config nor in the options.");
            throw new IllegalArgumentException("Configuration Manager is not ready, configuration was not loaded! This usually means that a valid gameId was not found neither in the config nor in the options.");
        }
    }

    public static void setOptions(NxSignalOptions options){
        if(options != null){
            //
            // Update options in the Activity Manager
            //
            NxActivityManager.getInstance().setOptions(options);
        }else{
            NxLogger.fatal(TAG, "[setOptions] NxSignalOptions option can not be null!!!");
            throw new NullPointerException("NxSignalOptions option can not be null!!!");
        }
    }

    public static void setGlobalDefaultValues(Map<String,Object> defaultValues){
        NxActivityManager.getInstance().setFieldDefaultValues(defaultValues);
    }

    public static Map<String,Object> getGlobalDefaultValues(){
        return NxActivityManager.getInstance().getOverwriteParameters();
    }

    public static void setNPSN(String npsn){
        if(npsn != null){
            NxActivityManager.getInstance().setNPSN(npsn);
            reportCustomEvent(JsonKeys.EVENT_NAME_SDK_LOGIN, new HashMap<String, Object>(), null);
        }else{
            NxLogger.warn(TAG, "[setNPSN] Tried to set null npsn");
        }
    }

    public static void reportLaunchEvent(NxLaunchType launchType){
        reportLaunchEvent(launchType, null);
    }

    public static void reportLaunchEvent(NxLaunchType launchType, Map<String,Object> optionalParameters){

        NxLogger.verbose(TAG, "[reportLaunchEvent] Function called.");

        NxSignalOptions options = NxActivityManager.getInstance().getOptions();
        if(options.isAutoTrackLaunchEvents() == true) {
            NxLogger.warn(TAG, "[reportLaunchEvent] Auto-tracking of launch events already occurring. Ignoring manual launch event calls.");
            return;
        }

        Map<String,Object> eventSpecificParameters;
        
        if(optionalParameters != null){
            eventSpecificParameters = new HashMap<String,Object>(optionalParameters);
        }else{
            eventSpecificParameters = new HashMap<String,Object>();
        }

        eventSpecificParameters.put(JsonKeys.EVENT_LAUNCH_TYPE, launchType.toString());
        
        reportCustomEvent(JsonKeys.EVENT_NAME_LAUNCH, eventSpecificParameters, null);
        
    }

    public static void reportRevenue(Map<String, Object> skuDetails, Map<String, Object> receipt){
        reportRevenue(skuDetails, receipt, null);
    }

    public static void reportRevenue(Map<String, Object> skuDetails, Map<String, Object> receipt, Map<String, Object> optionalParameters){

        NxLogger.verbose(TAG, "[reportRevenue] Function called.");
        if(skuDetails == null) {
            NxLogger.error(TAG, "[reportRevenue] Bundle SKUs is null. Dropping revenue event.");
            return;
        }

        if(receipt == null) {
            NxLogger.error(TAG, "[reportRevenue] Intent receipt is null. Dropping revenue event.");
            return;
        }

        if(!receiptDataMatchesSkuDetails(receipt, skuDetails)) {
            NxLogger.error(TAG, "[reportRevenue] Provided Bundle of skuDetails did not match what was in the Intent receipt data. Dropping revenue event.");
            return;
        }


        String currentJsonKey = "";
        try {
            JSONObject inAppPurchaseDataJson = getInAppPurchaseDataJson(receipt);
            currentJsonKey = "productId";
            String productId = inAppPurchaseDataJson.getString(currentJsonKey);
            JSONObject skuDetailsJson = getSkuDetailsJson(productId, skuDetails);
            currentJsonKey = "price";
            String displayedPrice = skuDetailsJson.getString(currentJsonKey);
            currentJsonKey = "price_currency_code";
            String priceLocale = skuDetailsJson.getString(currentJsonKey);
            currentJsonKey = "orderId";
            String receiptId = inAppPurchaseDataJson.getString(currentJsonKey);

            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put(JsonKeys.EVENT_REVENUE_STORE_ID, "Google Play");
            parameters.put(JsonKeys.EVENT_REVENUE_PAYMENT_TYPE, "currency");
            parameters.put(JsonKeys.EVENT_REVENUE_DISPLAYED_PRICE, displayedPrice);
            parameters.put(JsonKeys.EVENT_REVENUE_PRICE_LOCALE, priceLocale);
            parameters.put(JsonKeys.EVENT_REVENUE_IAP_SKU, productId);
            parameters.put(JsonKeys.EVENT_REVENUE_RECEIPT_ID, receiptId);

            if(optionalParameters != null) {
                parameters.putAll(optionalParameters);
            }

            Map<String, Object> extras = new HashMap<String, Object>();
            extras.put(JsonKeys.EVENT_REVENUE_SKU_DETAILS, skuDetails);
            extras.put(JsonKeys.EVENT_REVENUE_RECEIPT, receipt);
            String priceAmountMicros = skuDetailsJson.getString("price_amount_micros");
            double numericPrice = Double.parseDouble(priceAmountMicros);
            numericPrice /= 1000000;
            extras.put(JsonKeys.EVENT_REVENUE_NUMERIC_PRICE, numericPrice);
            reportCustomEvent(JsonKeys.EVENT_NAME_REVENUE, parameters, extras);

        } catch (JSONException e) {
            NxLogger.error(TAG, "[reportRevenue] Error extracting value from JSONObject with key " + currentJsonKey
            + " Dropping revenue event.");
        }
    }

    public static Map<String,Object> getClientMetadata(){
        Map<String,Object> returnMap = new HashMap<String,Object>(NxSDKGeneratedValues.getInstance().getParameters());

        returnMap.remove(JsonKeys.COMMON_PARAMETER_SIGNATURE);
        returnMap.remove(JsonKeys.COMMON_PARAMETER_TOY_SERVICE_ID);

        //
        // remove npsn if null or empty
        //
        if(returnMap.containsKey(JsonKeys.COMMON_PARAMETER_NPSN)) {

            String npsn = (String)returnMap.get(JsonKeys.COMMON_PARAMETER_NPSN);

            if(isNullOrEmptyString(npsn)) {
                returnMap.remove(JsonKeys.COMMON_PARAMETER_NPSN);
            }
        }

        returnMap.remove(JsonKeys.SPECIAL_PARAMETER_ENV);
        returnMap.remove(JsonKeys.COMMON_PARAMETER_APP_VERSION);
        returnMap.remove(JsonKeys.COMMON_PARAMETER_SDK_VERSION_CONFIG);
        returnMap.remove(JsonKeys.COMMON_PARAMETER_APP_TYPE);

        //
        // Add session_id
        //
        returnMap.put(JsonKeys.COMMON_PARAMETER_SESSION_ID, NxSDKGeneratedValues.getInstance().getSessionId());

        return returnMap;
    }

    public static String getJsonClientMetadata(){
        Map<String,Object> returnMap = NxSignal.getClientMetadata();
        JSONObject jo = new JSONObject(returnMap);
        return jo.toString();
    }

    private static boolean receiptDataMatchesSkuDetails(Map<String,Object> receipt, Map<String,Object> skuDetails) {
        boolean receiptDataMatchesSkuDetails = false;

        JSONObject receiptJson = getInAppPurchaseDataJson(receipt);
        String receiptProductId = null;
        if(receiptJson != null) {
            try {
                receiptProductId = receiptJson.getString("productId");
            } catch (JSONException e) {
                NxLogger.error(TAG, "[receiptDataMatchesSkuDetails] JSONObject of Intent receipt has no productId. " + e.getLocalizedMessage());
            }
        }
        String skuProductId = null;
        if(receiptProductId != null) {
            JSONObject skuJSON = getSkuDetailsJson(receiptProductId, skuDetails);
            if(skuJSON != null) {
                try {
                    skuProductId = skuJSON.getString("productId");
                } catch (JSONException e) {
                    NxLogger.error(TAG, "[receiptDataMatchesSkuDetails] JSONObject of retrieved sku from Bundle has no productId. " + e.getLocalizedMessage());
                }
            }
        }
        if(receiptProductId != null && skuProductId != null) {
            if(receiptProductId.equalsIgnoreCase(skuProductId)) {
                receiptDataMatchesSkuDetails = true;
            }
        }
        return receiptDataMatchesSkuDetails;
    }

    private static JSONObject getInAppPurchaseDataJson(Map<String,Object> receipt) {
        JSONObject inAppPurchaseDataJson = null;

        String inAppPurchaseDataString = (String)receipt.get("INAPP_PURCHASE_DATA");
        if(inAppPurchaseDataString == null) {
            NxLogger.error(TAG, "[getInAppPurchaseDataJson] Intent receipt has no INAPP_PURCHASE_DATA");
        }

        try {
            inAppPurchaseDataJson = new JSONObject(inAppPurchaseDataString);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[getInAppPurchaseDataJson] Error converting receipt string to JSONObject.");
        }

        return inAppPurchaseDataJson;
    }

    private static JSONObject getSkuDetailsJson(String productId, Map<String,Object> skuDetails) {
        JSONObject skuDetailsJson = null;
        ArrayList<String> detailsList = (ArrayList<String>)skuDetails.get("DETAILS_LIST");

        if(detailsList == null) {
            NxLogger.error(TAG, "[getSkuDetailsJson] SKUs bundle has no DETAILS_LIST array list.");
        }

        for(String skuString : detailsList) {
            try {
                JSONObject skuJson = new JSONObject(skuString);
                if(skuJson.getString("productId").equalsIgnoreCase(productId)) {
                    skuDetailsJson = skuJson;
                    break;
                }
            } catch (JSONException e) {
                NxLogger.error(TAG, "[getSkuDetailsJson] Error converting SKU string to JSONObject.");
            }
        }

        if(skuDetailsJson == null) {
            NxLogger.error(TAG, "[getSkuDetailsJson] SKUs bundle did not have any products that matched product ID " + productId);
        }

        return skuDetailsJson;
    }
    
    public static void reportEconEvent(NxSignalEconType eventType, String currency,
                                       Long amount, Long currentBalance, String source,
                                       String action, String target, Map<String, Object> optionalParameters){

        NxLogger.verbose(TAG, "[reportEconEvent] Function called.");
        Map<String, Object> parameters;
        if (optionalParameters != null) {
            parameters = new HashMap<String, Object>(optionalParameters);
        } else {
            parameters = new HashMap<String, Object>();
        }
        
        if(eventType == null){
            NxLogger.error(TAG, "[reportEconEvent] Economy type given is not handled.");
        }else{
        
            parameters.put(JsonKeys.EVENT_ECON_TYPE, eventType.toString());
            parameters.put(JsonKeys.EVENT_ECON_VIRTUAL_CURRENCY_TYPE, currency);
            parameters.put(JsonKeys.EVENT_ECON_VIRTUAL_CURRENCY_AMOUNT, amount);
            parameters.put(JsonKeys.EVENT_ECON_VIRTUAL_CURRENCY_BALANCE, currentBalance);
            parameters.put(JsonKeys.EVENT_ECON_SOURCE, source);
            
            if(action != null){
                parameters.put(JsonKeys.EVENT_ECON_ACTION, action);
            }
            
            if(target != null){
                parameters.put(JsonKeys.EVENT_ECON_TARGET, target);
            }
            
            reportCustomEvent(JsonKeys.EVENT_NAME_ECON, parameters, null);
        }
 
    }

    public static void reportCustomEvent(String eventType, Map<String, Object> optionalParameters, Map<String, Object> extras){

        if(eventType == null || eventType.isEmpty()) {
            NxLogger.error(TAG, "[reportCustomEvent] Event type is null or an empty string. Dropping custom event.");
            return;
        }

        NxActivityManager.getInstance().sendEvent(eventType, optionalParameters, extras);
    }

    /*
     *
     * Adjust Adapter specific methods
     *
     */
    public static void sendAdjustEventWithCallbackValues(String token, Map values){
        NxActivityManager.getInstance().sendAdjustEventWithCallbackValues(token, values);
    }

}
