/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.adapters;

import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.config.NxConfiguration;
import com.nexonm.nxsignal.event.NxPlatformDeviceIdInfo;
import com.nexonm.nxsignal.event.NxSDKGeneratedValues;
import com.nexonm.nxsignal.storage.NxDatabase;
import com.nexonm.nxsignal.storage.NxDatabaseRow;
import com.nexonm.nxsignal.NxActivityManager;
import com.nexonm.nxsignal.config.NxAdapterConfiguration;
import com.nexonm.nxsignal.config.NxAnalyticsConfiguration;
import com.nexonm.nxsignal.config.NxConfigurationManager;
import com.nexonm.nxsignal.event.NxEvent;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.networking.NxHttpService;
import com.nexonm.nxsignal.networking.NxHttpTask;
import com.nexonm.nxsignal.networking.NxHttpTaskHandler;
import com.nexonm.nxsignal.queue.DispatchQueue;
import com.nexonm.nxsignal.storage.NxStorage;

import java.net.HttpURLConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author David.Vallejo
 */
public class NxAdapterSignal extends NxAdapter implements NxHttpTaskHandler {

    enum FlushType {
        EVENT,
        TIMER,
        SIGNATURE
    }

    private static String TAG = "NxAdapterSignal";
    private static String ADAPTER_SIGNAL_NAME = "nxsignal";
    public static final String INTERNAL_QUEUE_NAME = "io.Nexon.NxAdapterSignal.queue";

    private static final String API_PATH_URL = "api/raw";

    private static final int SIGNATURE_REQUEST_COOLDOWN_SECS = 15;
    private static final String SIGNATURE_PATH_URL = "api/create_sig";

    //
    // Legacy : required to load events from old storage files, before we had a sqlite database.
    //
    private static final String KEY_INFLIGHT = "inflight";
    private static final String KEY_QUEUE = "queue";

    private static final String NX_EVENTS_STORAGE_FILE = "NxEvents.json";

    private static final String NX_PAYLOAD_VERSION = "2.0.0";

    private NxConfiguration configuration;
    private NxAnalyticsConfiguration analyticsConfiguration;
    private NxAdapterConfiguration adapterConfiguration;

    private long timeTillBatchSendingToServer = 60;
    private int maxEventsPerBatchSend;
    private int eventStorageLimit;
    private int numberOfEventsInQueueFlushThreshold;
    private int sendCooldown;

    private Timer flushTimer;
    private final Semaphore flushSemaphore = new Semaphore(1, true);
    private DispatchQueue internalQueue;

    //
    // The signature
    //
    private String signature = null;

    public NxAdapterSignal(String signature) {
        //
        // Set the adapter name
        //
        this.setAdapterIdentifier(ADAPTER_SIGNAL_NAME);

        this.signature = signature;
    }

    @Override
    public void start() {
        //
        // Avoid start more than once
        //
        if(!started) {
            //
            // Retrieve and store the config(s)
            //
            if (NxConfigurationManager.getInstance().isReady()) {
                //
                // If we got here, all the configs are VALID!
                //
                configuration = NxConfigurationManager.getInstance().getConfiguration();
                analyticsConfiguration = configuration.getAnalyticsConfiguration();
                adapterConfiguration = configuration.getAdapterConfiguration(getAdapterIdentifier());

                //
                // Read some useful values from the adapter config data
                //
                maxEventsPerBatchSend = adapterConfiguration.getMaxEventsPerBatchSend();
                eventStorageLimit = adapterConfiguration.getEventStorageLimit();
                numberOfEventsInQueueFlushThreshold = adapterConfiguration.getNumberOfEventsInQueueFlushThreshold();
                sendCooldown = adapterConfiguration.getSendCooldown();

                //
                // Obtain interval between batch sending for the event queue
                //
                timeTillBatchSendingToServer = adapterConfiguration.getTimeTillBatchSendingToServer() * 1000;

                //
                // Get or create the event queue
                //
                internalQueue = DispatchQueue.getQueueWithKey(INTERNAL_QUEUE_NAME, DispatchQueue.DrainStrategy.SERIAL);
                internalQueue.setPauseState(false);

                //
                // Legacy code required for backward compatibility - if the event storage file is present,
                // we do need to read it, get the events from there, store those events in the database and
                // then remove the file. This operation will be performed one time only, so we make it synchronous
                // because it doesn't really matter that much. And it is way easier this way.
                //
                recoverStorage();

                flushTimer = new Timer();
                flushTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        NxLogger.verbose(TAG, "[TimerTask.run] Timer elapsed, queueing flush event batch.");

                        //
                        // Force a flush, if required - this is a time trigger
                        //
                        flush(FlushType.TIMER);
                    }
                }, 0, timeTillBatchSendingToServer);

                //
                // Send signature request - without a valid signature we cannot send events!
                //
                this.signature = signature;

                started = true;
            } else {
                NxLogger.error(TAG, "[start] Cannot find adapter configuration and/or data. Cannot start adapter %s...", this.getAdapterIdentifier());
            }
        } else {
            NxLogger.warn(TAG, "[start] Adapter %s is already running...", this.getAdapterIdentifier());
        }
    }

    @Override
    public boolean isRunning() {

        if(configuration == null)
            return false;

        return started && (analyticsConfiguration != null) && (adapterConfiguration != null) && adapterConfiguration.isEnabled();
    }

    @Override
    public void sendEvent(NxEvent event) {

        //
        // First of all, store the event
        //
        storeEvent(event);

        //
        // Check to see if the event will trigger a flush
        //
        flush(FlushType.EVENT, event);
    }

    private void storeEvent(NxEvent event) {
        //
        // This is the public method used to send events. We want to store this event to the database.
        //
        // Save event to storage
        boolean stored = NxDatabase.getInstance().insertEvent(event);

        if(stored) {
            NxLogger.verbose(TAG, "[storeEvent] Event %s stored to database.", event.getType());

            checkStoredEventsLimit();

        } else {
            NxLogger.verbose(TAG, "[storeEvent] Failed to store event %s into database. Events count: %d", event.getType(), NxDatabase.getInstance().countEvents());
        }
    }

    private void flush(FlushType flushType) {
        flush(flushType, null);
    }

    private void flush(FlushType flushType, NxEvent event) {
        //
        // Check if we have the signature - which required in order
        // to send the event.
        //
        if(gotSignature()) {
            //
            // A trigger event occurred, time to flush the events out
            // OR
            // if the number of events currently exceeds the flush max queue
            //
            int eventsInDatabase = NxDatabase.getInstance().countEvents();

            if(eventsInDatabase > 0) {
                boolean justGotSignature = (flushType == FlushType.SIGNATURE);
                boolean isTimeTrigger = (flushType == FlushType.TIMER);
                boolean isFlushTrigger = (event != null) && adapterConfiguration.isFlushTrigger(event.getType());
                boolean isFlushThreshold = eventsInDatabase >= numberOfEventsInQueueFlushThreshold;
                if(justGotSignature || isTimeTrigger || isFlushTrigger || isFlushThreshold) {
                    internalQueue.add(new Runnable() {
                        @Override
                        public void run() {
                            sendEventsAsync();
                        }
                    });
                } else {
                    NxLogger.verbose(TAG,"[flush] Not a flush trigger event, or not enough events for flush threshold.");
                }
            } else {
                NxLogger.verbose(TAG,"[flush] Database is empty, nothing to flush!");
            }
        } else {
            NxLogger.verbose(TAG,"[flush] Cannot find a signature - not ready to flush events yet. Will retry later...");
        }
    }

    private void sendEventsAsync() {

        if(!flushSemaphore.tryAcquire()) {
            NxLogger.verbose(TAG, "[sendEventsAsync] Failed to acquire flush semaphore, event queue is already sending...");
            return;
        }

        //
        // Paranoid check: at this point we should have a signature for sure, but it does not hurt to check!
        //
        if(!gotSignature()) {
            NxLogger.verbose(TAG, "[sendEventsAsync] No signature found, cannot send event batch!");
            return;
        }

        NxLogger.verbose(TAG, "[sendEventsAsync] Flush semaphore acquired. Starting payload creation...");

        List<Long> companionData = new ArrayList<Long>(maxEventsPerBatchSend);
        JSONArray events = new JSONArray();

        //
        // Load the first 'maxEventsPerSend' events from the database, ordered by priority.
        //
        ArrayList<NxDatabaseRow> databaseRows = NxDatabase.getInstance().selectEvents(maxEventsPerBatchSend);

        if(databaseRows.size() == 0) {
            //
            // Release the semaphore and return if the database is empty!
            //
            flushSemaphore.release();

            NxLogger.verbose(TAG, "[sendEventsAsync] Nothing to flush, batch is empty. Flush semaphore released.");

            return;
        }

        for (int i = 0; i < databaseRows.size(); i++) {

            NxDatabaseRow databaseRow = databaseRows.get(i);

            //
            // This is the list of Ids of the events that we are sending with this batch.
            // It will be returned along with the HTTP handler callback response, so we know
            // which events to delete from the database.
            //
            companionData.add(databaseRow.getEventId());

            String eventBody = databaseRow.getEventBody();
            try {

                JSONObject eventBodyJson = new JSONObject(eventBody);

                //
                // Add signature!
                //
                eventBodyJson.put(JsonKeys.COMMON_PARAMETER_SIGNATURE, this.signature);

                //
                // Add platform_device_id
                //
                NxPlatformDeviceIdInfo platformDeviceIdInfo = NxSDKGeneratedValues.getInstance().getOrCreatePlatformDeviceIdInfo();

                eventBodyJson.put(JsonKeys.COMMON_PARAMETER_PLATFORM_DEVICE_ID, platformDeviceIdInfo.getId());
                eventBodyJson.put(JsonKeys.COMMON_PARAMETER_PLATFORM_DEVICE_ID_TYPE, platformDeviceIdInfo.getIdType());

                events.put(eventBodyJson);

            } catch (JSONException e) {
                NxLogger.error(TAG,"[sendEventsAsync] Failed to create JSON Object for event:\nevent_id: %d\nevent_type: %s\nevent_priority: %d\nevent_body: %s",databaseRow.getEventId(),databaseRow.getEventType(),databaseRow.getPriority(),databaseRow.getEventBody() != null ? databaseRow.getEventBody() : "<NULL>");
            }

            //
            // NOTE : we want to add the event_id to the companion data anyway: if the body is
            // malformed JSON, we just want to delete that event sooner or later anyway.
            //
        }

        NxHttpService http = NxHttpService.getInstance();
        String jsonPayload = null;

        String environment = NxActivityManager.getInstance().getOptions().getEnvironment();
        String appId = analyticsConfiguration.getAppId();

        String url = getServerUrl(API_PATH_URL);

        //
        // TODO: implement compression case (just like iOS)
        //

        JSONObject eventBatchJson = new JSONObject();
        try {
            eventBatchJson.put("nxa", appId);
            eventBatchJson.put("nxenv", environment);
            eventBatchJson.put("e", events);
            eventBatchJson.put("nxv", NX_PAYLOAD_VERSION);
        } catch (JSONException e) {
            NxLogger.error(TAG, "[sendEventsAsync] Error creating JSONObject of batched events. " + e.getMessage());
        }

        jsonPayload = eventBatchJson.toString();
        NxLogger.verbose(TAG, "[sendEventsAsync] Payload:\n %s", jsonPayload);
        http.sendPostJson(url, jsonPayload, this, companionData);
    }

    private void checkStoredEventsLimit() {

        int countEvents = NxDatabase.getInstance().countEvents();

        //
        // Check if we have reached the max event limit already. If so, we drop the oldest ones from the database.
        //
        if(countEvents > eventStorageLimit) {
            NxLogger.warn(TAG, "[sendEvent] Dropping events as storage is size %d but limit is %d.", countEvents, eventStorageLimit);
            this.dropEvents();
        } else {
            NxLogger.verbose(TAG, "[sendEvent] There are %d stored events and the limit is %d, no need to drop any events.", countEvents, eventStorageLimit);
        }
    }

    private void dropEvents() {
        NxDatabase.getInstance().deleteEvents((int)(eventStorageLimit * 0.2));
        NxLogger.verbose(TAG, "[dropEvents] There are %d remaining events after the drop.", NxDatabase.getInstance().countEvents());
    }

    private void onSuccessfulEventsSend(NxHttpTask httpTask) {
        NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Event successfully sent.");

        int statusCode = httpTask.getStatusCode();

        NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Status Code: %d", statusCode);
        NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Body Response: %s", httpTask.getStringBody());

        //
        // Companion data contains the list of event_id included in this batch.
        //
        List<Long> companionData = (List<Long>) httpTask.getCompanionData();

        if( statusCode >= 500 ) {
            //
            // Server problem, will retry later...
            //
            NxLogger.warn(TAG, "[handleSuccessfulHttpTaskCallback] Got %s response when sending events to %s. Will retry later...", statusCode,
                    httpTask.getTaskUrl());
        } else {
            //
            // Not a server problem, or successfully sent. In both cases we remove the events from the database and that's it.
            //
            if(httpTask.getStatusCode() != HttpURLConnection.HTTP_OK) {
                NxLogger.error(TAG, "[handleSuccessfulHttpTaskCallback] Got %s response when sending events to %s so events have been dropped and will not be resent",
                        statusCode,
                        httpTask.getTaskUrl());
            }

            boolean success = NxDatabase.getInstance().deleteEvents(companionData);

            if (success) {
                NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Events deleted from database. Events in database: " + NxDatabase.getInstance().countEvents());
            } else {
                NxLogger.error(TAG, "[handleSuccessfulHttpTaskCallback] Failed to delete events from database. Events in database: " + NxDatabase.getInstance().countEvents());
            }
        }

        //
        // Release the semaphore!
        //
        flushSemaphore.release();

        NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Flush semaphore released.");
    }

    private void onFailedEventsSend(NxHttpTask httpTask) {

        int statusCode = httpTask.getStatusCode();

        NxLogger.error(TAG, "[handleFailedHttpTaskCallback] Failed");
        NxLogger.error(TAG, "[handleFailedHttpTaskCallback] Status Code: %d", statusCode);

        List<Long> companionData = (List<Long>) httpTask.getCompanionData();

        NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Number of events in payload %d", companionData.size());

        if(500 <= statusCode && statusCode < 600) {

            NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Stopping further event queue tasks from running due to 5XX errors.");
            // Stop any further eventQueue tasks from running because clearly the server is sick now
            internalQueue.clear();

        } else {
            // A HTTP failure occurred that isn't a 500. Drop the in flight events as 500's are the only ones we
            // will retry on.
            NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Dropping current batch of in flight events.");

            boolean success = NxDatabase.getInstance().deleteEvents(companionData);

            if(success) {
                NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Events deleted from database. Events in database: " + NxDatabase.getInstance().countEvents());
            } else {
                NxLogger.error(TAG, "[handleFailedHttpTaskCallback] Failed to delete events from database. Events in database: " + NxDatabase.getInstance().countEvents());
            }
        }

        //
        // Release the semaphore!
        //
        flushSemaphore.release();

        NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Flush semaphore released.");
    }

    private boolean gotSignature() {
        return this.signature != null;
    }

    public void updateSignature(String pSignature) {
        this.signature = pSignature;

        if(this.signature != null) {
            NxLogger.verbose(TAG, "[updateSignature] Signature updated: %s", this.signature);

            flush(FlushType.SIGNATURE);
        } else {
            NxLogger.warn(TAG, "[updateSignature] Signature updated, but set to null!");
        }
    }

    private String getServerUrl(String apiPath) {

        String environment = NxActivityManager.getInstance().getOptions().getEnvironment();
        String url = analyticsConfiguration.getApiServerUrl(environment);

        return url + apiPath;
    }

    // ---------------------------------------------------------------------------------------------
    // NxHttpTaskHandler overrides
    // ---------------------------------------------------------------------------------------------
    @Override
    public void handleSuccessfulHttpTaskCallback(NxHttpTask httpTask) {
        if(httpTask.getTaskUrl().equalsIgnoreCase(getServerUrl(API_PATH_URL))) {
            onSuccessfulEventsSend(httpTask);
        }
    }

    @Override
    public void handleFailedHttpTaskCallback(NxHttpTask httpTask) {
        if(httpTask.getTaskUrl().equalsIgnoreCase(getServerUrl(API_PATH_URL))) {
            onFailedEventsSend(httpTask);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Legacy Methods - Required for backward compatibility and to patch existing apps
    // ---------------------------------------------------------------------------------------------
    private void recoverStorage() {

        String path = NX_EVENTS_STORAGE_FILE;

        if(NxStorage.getInstance().fileExists(path)) {

            NxLogger.info(TAG, "[recoverStorage] Legacy %s storage file still exists. Loading the data...", path);

            String content = NxStorage.getInstance().loadStringFromFile(path, "UTF-8");

            if (content != null) {
                try {
                    // Convert the string to JSONObject
                    JSONObject saving = new JSONObject(content);

                    // Get the JSONArray of the in flight and queue
                    JSONArray saveInFlight = saving.getJSONArray(KEY_INFLIGHT);
                    JSONArray queue = saving.getJSONArray(KEY_QUEUE);

                    // In case there weren't any events in either of the queues, make a default empty JSONArray
                    if (saveInFlight == null) {
                        saveInFlight = new JSONArray();
                    }

                    if (queue == null) {
                        queue = new JSONArray();
                    }

                    NxLogger.verbose(TAG, "[recoverStorage] To be loaded %d in flight and %d priority queue from storage. In total, %d events will be added to the database.",
                            saveInFlight.length(), queue.length(), saveInFlight.length() + queue.length());

                    // NOTE: The contents of the objects in the arrays are String for some reason. Need to obtain String form
                    // and convert them to JSONObject.

                    // Get all the events from save in flight and from ones in storage and resend them into the
                    // event function
                    JSONObject rawEvent;
                    for (int i = 0; i < saveInFlight.length(); i++) {
                        String jsonContent = saveInFlight.getString(i);
                        rawEvent = new JSONObject(jsonContent);
                        this.sendEvent(new NxEvent(rawEvent));
                    }

                    for (int i = 0; i < queue.length(); i++) {
                        String jsonContent = queue.getString(i);
                        rawEvent = new JSONObject(jsonContent);
                        this.sendEvent(new NxEvent(rawEvent));
                    }

                } catch (JSONException ex) {
                    NxLogger.error(TAG, "[recoverStorage] Error loading storage file %s", ex.getMessage());
                } finally {
                    //
                    // In any case, we get rid of the file, assuming that if its content was not ok once,
                    // it will never be ok ever.
                    //
                    NxStorage.getInstance().deleteFile(path);
                }
            }
        } else {
            NxLogger.verbose(TAG, "[recoverStorage] Legacy %s storage file does not exist. Skipping...", path);
        }
    }
}
