package com.nexonm.nxsignal.adapters;

import com.nexonm.nxsignal.config.NxConfiguration;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.config.NxAdapterConfiguration;
import com.nexonm.nxsignal.config.NxConfigurationManager;
import com.nexonm.nxsignal.event.NxEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public class NxAdapterManager{
    
    private static final String TAG = "NxAdapterManager";
    
    private Map<String,NxAdapter> adapterMap;

    private static NxAdapterManager sharedInstance = null;

    private Object adaptersHaveStartedLock = new Object();
    private boolean adaptersHaveStarted = false;
    
    private NxAdapterManager(){
        adapterMap = new HashMap<String,NxAdapter>();
    }

    public static synchronized NxAdapterManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxAdapterManager();
        }
        return sharedInstance;
    }

    public void sendEvent(NxEvent event) {

        NxLogger.verbose(TAG, "[sendEvent] (adapterArray.count=%d)", adapterMap.size());
        
        //Note: its done this way so that if the map is modified while we are in the loop nothing breaks
        Set adapterKeys = adapterMap.keySet();
        for (Object key : adapterKeys) {
            NxAdapter adapter = adapterMap.get(key.toString());
            if(adapter != null && adapter.isRunning()){
                NxLogger.info(TAG, "Event %s forwarded to adapter: %s", event.getType(), adapter.getAdapterIdentifier());
                adapter.sendEvent(event);
            }
            //else not there anymore so don't send it (no error required)
        }
    }

    public void addAdapter(NxAdapter adapter) {
        NxLogger.info(TAG, "[addAdapter] Adapter added: %s", adapter.getAdapterIdentifier());
        
        NxAdapter oldAdapter = adapterMap.put(adapter.getAdapterIdentifier(), adapter);
        
        if(oldAdapter != null){
            NxLogger.warn(TAG, "[addAdapter] Overwrote existing adapter: %s", adapter.getAdapterIdentifier());
        }
        
    }

    public NxAdapter getAdapter(String adapterName) {
        return this.adapterMap.get(adapterName);
    }

    public void startAdapters() {
        NxConfiguration config = NxConfigurationManager.getInstance().getConfiguration();
        if (config == null) {
            NxLogger.error(TAG, "[%s] No configuration given.", "startAdapters");
            return;
        }

        Map<String, NxAdapterConfiguration> adapterConfigurations = config.getAdapterConfigurations();
        if (adapterConfigurations == null) {
            NxLogger.error(TAG, "[%s] No adapter configurations found.", "startAdapters");
        }

        NxLogger.verbose(TAG,"[%s] (adapter count:%d)", "startAdapters", adapterMap.size());

        // Take the given adapter configurations and match them with the existing adapters that registered themselves.
        // Names of the adapters should match on the config and self registration end.
        for(String adapterIdentifier : adapterConfigurations.keySet()) {
            NxAdapter currentAdapter = adapterMap.get(adapterIdentifier);
            NxAdapterConfiguration adapterConfig = adapterConfigurations.get(adapterIdentifier);

            if(currentAdapter == null) {
                NxLogger.warn(TAG, "[%s] Adapter %s never registered with NxAdapterManager.", "startAdapters", adapterIdentifier);
            }

            if(adapterConfig == null) {
                NxLogger.warn(TAG, "[%s] Adapter %s does not have an adapter configuration.", "startAdapters", adapterIdentifier);
            }

            // If the config says this adapter is enabled, set the config into the adapter and start it up
            if(currentAdapter != null && adapterConfig != null && adapterConfig.isEnabled()) {
                currentAdapter.start();
            } else if(currentAdapter != null && adapterConfig != null) {
                NxLogger.info(TAG, "[%s] Adapter %s is not enabled. Skipping...", "startAdapters", adapterIdentifier);
            }
        }

        this.setAdaptersHaveStarted(true);
    }

    private void setAdaptersHaveStarted(boolean neoValue){
        synchronized (adaptersHaveStartedLock){
            adaptersHaveStarted = neoValue;
        }
    }
}
