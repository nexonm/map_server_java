/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexonm.nxsignal.adapters;

import com.nexonm.nxsignal.event.NxEvent;

/**
 *
 * @author David.Vallejo
 */
abstract public class NxAdapter {

    protected static final String TAG = "NxAdapter";
    private String adapterIdentifier;
    protected boolean started = false;

    public NxAdapter() {
        adapterIdentifier = "";
    }

    public void setup() {
    }

    abstract public void start();

    abstract public void sendEvent(NxEvent event);

    public String getAdapterIdentifier() {
        return adapterIdentifier;
    }

    abstract public boolean isRunning();

    protected void setAdapterIdentifier(String adapterIdentifier) {
        this.adapterIdentifier = adapterIdentifier;
    }
}
