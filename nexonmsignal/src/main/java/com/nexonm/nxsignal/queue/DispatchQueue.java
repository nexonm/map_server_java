/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.queue;

import com.nexonm.nxsignal.logging.NxLogger;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This is the class to use if you want things done in an asynchronous queue. The queue processor is the
 * protected component.
 *
 * Created by David.Vallejo on 2/3/16.
 */
public class DispatchQueue {

    private static String TAG = "DispatchQueue";
    private Object _lock = new Object();
    public enum DrainStrategy{
        NONE,
        SERIAL
    }
        
    public static DispatchQueue getQueueWithKey(String key, DrainStrategy strategy){
        return QueueProcessor.getQueueWithKey(key, strategy,null);
    }

    public static DispatchQueue getQueueWithKey(String key, DrainStrategy strategy,Long flushInterval){
        return QueueProcessor.getQueueWithKey(key, strategy,flushInterval);
    }

    private final DrainStrategy queueStrategy;
    
    private final List<Runnable> queue;
    private final String queueKey;
    private Long flushInterval;
    private boolean paused;

    protected DispatchQueue(String queueKey,DrainStrategy strategy){
        this(queueKey,strategy,null);
    }


    protected DispatchQueue(String queueKey, DrainStrategy strategy, Long flushInterval){
        queueStrategy = strategy;
        this.queueKey = queueKey;
        this.flushInterval = flushInterval;
        paused = true;
        if(queueStrategy == DrainStrategy.SERIAL) {
            queue = Collections.synchronizedList(new LinkedList());
        } else {
            // TODO 04-11-2016 Vince: if the strategy type is not serial, this queue is always null, which makes me
            // wonder how do you add tasks to it since it'll blow up in the add function below.
            queue = null;
        }
    }
    
    public void add(Runnable runnable) {
        if(queue != null) {
            queue.add(runnable);
            if (!this.hasFlushInterval()) {
                QueueProcessor.getInstance().processQueue(this.queueKey);
            }
        } else {
            NxLogger.error(TAG, "[add] Queue %s was never created. Cannot add to.", queueKey);
        }
    }

    public void flush(){
        QueueProcessor.getInstance().processQueue(this.queueKey);
    }

    public DrainStrategy getStrategy(){
        return queueStrategy;
    }

    
    protected Runnable popQueue(){
        synchronized (_lock) {
            if (queue != null && !queue.isEmpty()) {
                return queue.remove(0);
            } else if (queue != null && queue.isEmpty()) {
                NxLogger.error(TAG, "[popQueue] Tried to pop an empty queue.");
            }
            return null;
        }
    }
    
    public boolean isEmpty(){
        if(queue != null) {
            return queue.isEmpty();
        }
        return true;
    }

    
    public void setPauseState(boolean state){
        paused = state;
        if(!paused){
            QueueProcessor.getInstance().processQueue(this.queueKey);
        }
    }
    
    public boolean isPaused() {
        return paused;
    }
    
    public int count() {
        if(queue != null) {
            return queue.size();
        }
        return 0;
    }

    public boolean hasFlushInterval(){
        return  flushInterval != null && flushInterval>0;
    }

    public Long getFlushInterval(){
        return flushInterval;
    }

    public String getQueueKey() {
        return queueKey;
    }

    public void clear() {
        synchronized (_lock) {
            if (queue != null) {
                queue.clear();
            }
        }
    }
}
