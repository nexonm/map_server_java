/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.queue;

import com.nexonm.nxsignal.logging.NxLogger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Use the dispatch queue for asynchronous tasks. This class is just the component.
 *
 * @author David.Vallejo
 */
class QueueProcessor {

    private static String TAG = "QueueProcessor";
    
    protected static synchronized QueueProcessor getInstance(){
        if (sharedInstance == null){
            sharedInstance = new QueueProcessor();
        }
        return sharedInstance;
    }
    
    private static QueueProcessor sharedInstance;
    
    static DispatchQueue getQueueWithKey(String key, DispatchQueue.DrainStrategy strategy, Long interval){
        QueueProcessor instance = getInstance();
        
        return getQueueWithKey(instance, key, strategy,interval);

    }
    
    private static synchronized DispatchQueue getQueueWithKey(QueueProcessor instance, String key,
                                                              DispatchQueue.DrainStrategy strategy,Long interval) {

        DispatchQueue result = instance.queues.get(key);

        if (result == null) {
            NxLogger.verbose(TAG, "[getQueueWithKey] Creating new queue %s", key);
            result = new DispatchQueue(key, strategy, interval);
            instance.queues.put(key, result);

            if(result.hasFlushInterval()) {
                // TODO 04-10-16 Vince: already have an instance of the QueueProcessor, why are you creating another reference?
                QueueProcessor qProc = getInstance();
                ProcessingQueueTask timedTask = new ProcessingQueueTask(key, true);
                NxLogger.verbose(TAG, "[getQueueWithKey] Queue %s has a flush interval of %d milliseconds", key, result.getFlushInterval());
                qProc.scheduledTimer.scheduleAtFixedRate(timedTask, 0, result.getFlushInterval());

            }

        }

        return result;
    }
    
    private final ExecutorService threadPool;
    
    private Map<String,DispatchQueue> queues;
    private Map<String,ProcessingQueueTask> processQueueTasks;
    private Timer scheduledTimer;
    
    private QueueProcessor(){
        threadPool = Executors.newCachedThreadPool();
        queues = Collections.synchronizedMap(new HashMap());
        processQueueTasks = Collections.synchronizedMap(new HashMap());

        scheduledTimer = new Timer();

    }

    private void process(Runnable task){
        threadPool.execute(task);
    }

    protected synchronized void processQueue(String queueWithKey) {
        DispatchQueue queue = queues.get(queueWithKey);
        if (queue != null && !queue.isPaused()) {
            if (!processQueueTasks.containsKey(queueWithKey)) {
                ProcessingQueueTask serialTask = new ProcessingQueueTask(queueWithKey,false);
                processQueueTasks.put(queueWithKey, serialTask);
                process(serialTask);
            }
        }
    }

    // Executes all the tasks for a specific queue
    private static class ProcessingQueueTask extends TimerTask {

        private static String TAG = "ProcessingQueueTask";
        private final String queueKey;
        private final boolean isTimer;

        ProcessingQueueTask(String key,boolean isTimer){
            queueKey = key;
            this.isTimer = isTimer;
        }

        @Override
        public void run() {


            QueueProcessor qProc = getInstance();
            
            DispatchQueue queue = qProc.queues.get(queueKey);

            //NxLogger.verbose(TAG, "[run] Checking queue %s with size %s", queue.getQueueKey(), queue.count());
            while(!queue.isPaused() && !queue.isEmpty()){
                Runnable task;
                //NxLogger.verbose(TAG, "[run] Emptying queue %s", queue.getQueueKey());
                if (queue.getStrategy() == DispatchQueue.DrainStrategy.SERIAL) {
                    //NxLogger.verbose(TAG, "[run] Queue has serial strategy.");
                    if(isTimer){
                        //NxLogger.verbose(TAG, "[run] Task uses a timer.");

                        // All this does is make another Processing Queue tasks that processes the dispatch queue
                        // with that key serially.....
                        qProc.processQueue(queueKey);

                        // Remove this comment once verified - Vince
                        // Fix for crazy looping of timer task, probably causing the message spamming.
                        break;
                    }else {
                        //NxLogger.verbose(TAG, "[run] Task does not use a timer.");
                        task = queue.popQueue();
//                        try {
                            if(task != null) {
                                task.run();
                            }
//                        }catch(Exception ex){
//                            NxLogger.error(TAG,ex.getMessage());
//                        }
                    }
                }else{
                    //NxLogger.verbose(TAG, "[run] Queue has shotgun strategy.");
                    task = queue.popQueue();
                    if(task != null) {
                        qProc.process(task);
                    }
                }
            }
            //NxLogger.verbose(TAG, "[run] Removing queue from process queue tasks.");
            qProc.processQueueTasks.remove(queueKey);
        }
    }
    
    
}
