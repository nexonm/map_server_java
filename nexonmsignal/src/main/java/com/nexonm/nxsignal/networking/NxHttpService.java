/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.networking;

import com.nexonm.nxsignal.config.NxConfigurationManager;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.queue.DispatchQueue;
import com.nexonm.nxsignal.storage.NxStorageLoadTaskHandler;
import com.nexonm.nxsignal.storage.NxStorage;
import com.nexonm.nxsignal.storage.NxStorageSaveTask;
import com.nexonm.nxsignal.storage.NxStorageSaveTaskHandler;
import com.nexonm.nxsignal.storage.NxStorageTask;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Map;

/**
 *
 * @author David.Vallejo
 */
public class NxHttpService {
    private static final String TAG = "NxHttpService";

    private static final String STORAGE_FILE_NAME = "NxStorage";
    private static final String DISPATCH_QUEUE_NAME = "io.Nexon.HttpService.Queue";

    private static NxHttpService sharedInstance = null;
    
    public static synchronized NxHttpService getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NxHttpService();
        }
        return sharedInstance;
    }
    
    private DispatchQueue internalQueue;
    
    private NxHttpService(){
        internalQueue = DispatchQueue.getQueueWithKey(DISPATCH_QUEUE_NAME, DispatchQueue.DrainStrategy.SERIAL);
        internalQueue.setPauseState(false);
    }
    
    public NxHttpTask sendPostJson(String url, String payload,NxHttpTaskHandler handler,Object companionData){
        NxHttpTask task = null;
        if(handler != null){
            task = new NxHttpTask(NxHttpTask.RequestType.POST,url,null,payload,handler,companionData);
            internalQueue.add(task);
        }
        return task;
    }
    
    public NxHttpTask sendGet(String url, Map<String,String> params, NxHttpTaskHandler handler,Object companionData){
        NxHttpTask task = null;
        if(handler != null){
            task = new NxHttpTask(NxHttpTask.RequestType.GET, url, params, null, handler, companionData);
            internalQueue.add(task);
        }
        return task;
    }

    // TODO 04-28-2016 Vince: This always assumed you accepted a config file and stored it. 204 is the exception!!!
    public NxHttpTask streamToFile(String url,Map<String,String> params,NxStorageLoadTaskHandler handler,
                                   String savePath,Object companionData){
        NxHttpTask task = null;
        if(handler != null) {
            FileStreamer httpHandler = new FileStreamer(savePath,handler);
            task = new NxHttpTask(NxHttpTask.RequestType.GET, url, params, null, httpHandler, companionData);
            NxStorage.getInstance().streamFile(httpHandler);
            internalQueue.add(task);
        }
        return task;
    }
    
    
    public class FileStreamer implements NxHttpTaskHandler,NxStorageSaveTaskHandler {
        
        private NxStorageLoadTaskHandler originalRequestor;
        private NxStorageTask fileSaver;
        private String savePath;
        private NxHttpTask httpTask;

        private FileStreamer(String savePath, NxStorageLoadTaskHandler handler){
            this.originalRequestor = handler;
            this.savePath = savePath;
        }

        public String getSavePath() {
            return savePath;
        }

        public void setStorageTask(NxStorageTask fileSaver) {
            this.fileSaver = fileSaver;
        }
        
        @Override
        public void handleSuccessfulHttpTaskCallback(NxHttpTask task) {

            httpTask = task;

            NxLogger.verbose(TAG, "[handleSuccessfulHttpTaskCallback] Http success with status code: %d", httpTask.getStatusCode());
            if(httpTask.getStatusCode() == HttpURLConnection.HTTP_OK) {
                //call run on the file saver it should callback this object
                fileSaver.run();
            } else {

                // TODO vince 4-10-2016: Quick fix of the else block here added. Need to review if this is the best way to address a non 200 status code
                handleFailedSaveTaskCallback((NxStorageSaveTask) fileSaver);
            }
        }
        
        @Override
        public void handleFailedHttpTaskCallback(NxHttpTask task) {
            //TODO: not pretty fix me

            // A file save should not occur in an HTTP failure. Will overwrite the config with junk? Vince
            httpTask = task;

            NxLogger.verbose(TAG, "[handleFailedHttpTaskCallback] Http response with status code: %d", httpTask.getStatusCode());
            
            //fileSaver.run();
            // TODO vince 4-10-2016: Quick fix in the failure http scenario. Need to review if this is the best way to address this.
            handleFailedSaveTaskCallback((NxStorageSaveTask) fileSaver);
        }

        @Override
        public void handleSuccessfulSaveTaskCallback(NxStorageSaveTask saveTask) {

            InputStream rawDownloadStream = httpTask.getStream();

            OutputStream rawSaveStream = saveTask.getRawStream();

            BufferedOutputStream saveStream = new BufferedOutputStream(rawSaveStream);

            int bytesRead = 0;
            //int totalBytes = 0;
            byte[] buffer = new byte[32768];// 32K
            try {
                while (rawDownloadStream != null && (bytesRead = rawDownloadStream.read(buffer)) != -1) {
                    saveStream.write(buffer,0,bytesRead);
                    //totalBytes = totalBytes+bytesRead;
                }
                saveStream.flush();
                //NxLogger.verbose(TAG, "****************** TOTAL BYTES STREAMED " + totalBytes + " / " + httpTask.getContentSize() + " SC:"+httpTask.getStatusCode());
            } catch (IOException ex) {
                NxLogger.error(TAG, ex.getMessage());
            }

            //TODO send queue up a setup task
            NxStorage.getInstance().loadFromFile(this.savePath, originalRequestor,httpTask.getCompanionData());

        }        

        @Override
        public void handleFailedSaveTaskCallback(NxStorageSaveTask task) {
            //TODO: not pretty fix me
            NxStorage.getInstance().loadFromFile(this.savePath, originalRequestor,null);
        }
    }
  
}
