/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.networking;

/**
 *
 * @author David.Vallejo
 */
public interface NxHttpTaskHandler {

    public void handleSuccessfulHttpTaskCallback(NxHttpTask task);
    
    public void handleFailedHttpTaskCallback(NxHttpTask task);

}
