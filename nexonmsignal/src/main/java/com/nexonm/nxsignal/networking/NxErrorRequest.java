package com.nexonm.nxsignal.networking;

import com.nexonm.nxsignal.NxActivityManager;
import com.nexonm.nxsignal.NxSignal;
import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxDeviceInfo;
import com.nexonm.nxsignal.utils.NxUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;

/**
 * Created by jackyho on 8/4/16.
 */
public class NxErrorRequest implements Runnable {
    private static final String TAG = "NxErrorRequest";

    private String errorMessage;
    private int errorCode;
    private String urlString;
    private String appId;
    private String environment;

    public NxErrorRequest(String errorMessage, int errorCode, String urlString, String appId, String environment) {

        this.errorMessage = errorMessage;
        this.errorCode = errorCode;

        this.urlString = urlString;
        this.appId = appId;
        this.environment = environment;

    }

    private String makePayload(String errorMessage, int errorCode, String appId, String environment) {

        String payload = new String();
        JSONObject eventBatchJson = new JSONObject();
        try {

            eventBatchJson.put("nxa", appId);
            eventBatchJson.put("env", environment);
            eventBatchJson.put("message", errorMessage);
            eventBatchJson.put("code", errorCode);
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_SDK_VERSION, NxUtils.getSdkVersion());
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_DEVICE_MAKE, NxDeviceInfo.getInstance().getDeviceMake());
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_DEVICE_MODEL, NxDeviceInfo.getInstance().getDeviceModel());
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_DEVICE_NAME, ""); // YES, we do need the parameter as it is marked as required, but we don't want to send the value, so we keep it empty.
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_OS_PLATFORM, NxDeviceInfo.getInstance().getOSPlatform());
            eventBatchJson.put(JsonKeys.COMMON_PARAMETER_OS_NAME, NxDeviceInfo.getInstance().getOSName());


        } catch (JSONException ex) {
            NxLogger.error(NxLogger.NEXON_TAG, "[makePayload] JSONException occurred " + ex.getLocalizedMessage());
        }

        payload = eventBatchJson.toString();
        return payload;
    }

    public void run() {
        sendError();
    }


    public void sendError() {
        String payload = makePayload(this.errorMessage, this.errorCode, this.appId, this.environment);
        try {
            URL url = new URL(this.urlString + "api/error");

            HttpURLConnection result = (HttpURLConnection) url.openConnection();

            result.setRequestMethod("POST");
            result.setDoInput(true);
            result.setDoOutput(true);

            OutputStream os = result.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.write(payload);
            writer.flush();
            writer.close();
            os.close();
        }
        catch (IOException ex) {
            NxLogger.error(NxLogger.NEXON_TAG, "[sendError] IOException occurred with error http request "+ urlString + " " + ex.getLocalizedMessage());
        }
        catch (Exception ex) {
            NxLogger.error(NxLogger.NEXON_TAG, "[sendError] Exception occurred with error http request "+ urlString + " " + ex.getLocalizedMessage());
        }
    }
}
