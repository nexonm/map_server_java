/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.networking;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.queue.NxTaskStatus;
import com.nexonm.nxsignal.utils.NxUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Map;

/**
 *
 * @author David.Vallejo
 */
public class NxHttpTask implements Runnable, NxTaskStatus {
    
    private static final String TAG = "NxHttpTask";

    public enum HttpType {

        HTTPS("https://", 1),
        HTTP("http://", 2);

        public static HttpType getType(String url) {
            if(url.toLowerCase().startsWith(HTTP.strValue)){
                return HTTP;
            }else if(url.toLowerCase().startsWith(HTTPS.strValue)){
                return HTTPS;
            }else{
                throw new IllegalArgumentException("Url should start with https:// or http://");
            }
        }

        private int intValue;
        private String strValue;

        private HttpType(String neoStringValue, int neoIntValue) {
            intValue = neoIntValue;
            strValue = neoStringValue;
        }

        @Override
        public String toString() {
            return strValue;
        }

        public int toInt() {
            return intValue;
        }

    }

    public enum RequestType {
        GET,
        POST,
        PUT,
        DELETE
    }
    
    //NOTE: This is part of a android workaround in the case that the servers
    //      that have been posted too have keep alive on and so connections
    //      go stale and so they need to be cleared.
    private static int MAX_CONNECTIONS = 5;
    static {
        //System.setProperty("http.maxConnections", String.valueOf(MAX_CONNECTIONS));
        if(System.getProperty("http.maxConnections") != null) {
            MAX_CONNECTIONS = Integer.parseInt(System.getProperty("http.maxConnections"));
        }
    }
    
    
    private String taskUrl;
    private NxHttpTaskHandler handler;
    private HttpType httpRequestType;
    
    private InputStream stream;
    private int streamSize;
    
    private float percentDownloaded;
    private long lastUpdate ;
    private int statusCode;
    private int contentSize;
    private FileChannel fileInfo;
    private Object companionData;
    private boolean running = false;
    private boolean valid = false;
    private String payload;
    RequestType reqType;
    private Map<String, String> urlParams;
    private String message;

    NxHttpTask(RequestType reqType, String url, Map<String, String> urlParams, String payload,
               NxHttpTaskHandler handler, Object companionData) {
        this.taskUrl = url;
        this.handler = handler;
        this.httpRequestType = HttpType.getType(this.taskUrl);
        this.companionData = companionData;
        this.payload = payload;
        this.reqType = reqType;
        this.urlParams = urlParams;
                     
        percentDownloaded = 0;
        lastUpdate = 0;
    }
    
    @Override
    public void run() {
        if(!running){
            this.forceOneRunOnObjectAtATime();
        }
    }
    
    private void reset(){
        this.streamSize = 1;
        
        if(stream != null){
            try {
                stream.close();
            } catch (IOException ex) {
                NxLogger.error(TAG, ex.getMessage());
            }
        }
    }
    
    private void handleFailedCase(){
        if(statusCode <= 0){
            statusCode = HttpURLConnection.HTTP_UNAVAILABLE;
        }
        try {
            this.handler.handleFailedHttpTaskCallback(this);
        }catch(Exception ex){
            NxLogger.error(TAG, ex.getMessage());
        }
    }
    
    private synchronized void forceOneRunOnObjectAtATime() {
        running = true;
        //TODO: maybe error out on second run?
        if (handler != null) {
            HttpURLConnection connection = null;
            int failures = 0;
            boolean dataRead= false;
            for (int i = 0; i < MAX_CONNECTIONS+1 && !dataRead; i++) {
                try {
                    // initialize connection...
                    connection = this.createConnection(failures > 0 && failures <= MAX_CONNECTIONS);
                    if(connection != null){

                        NxLogger.verbose(TAG, "[forceOneRunOnObjectAtATime] Sending payload.");
                        connection.connect();

                        statusCode = connection.getResponseCode();
                        contentSize = connection.getHeaderFieldInt("Content-Length", 1);
                        message = null;
                        if(statusCode == 200){
                            stream = connection.getInputStream();
                        } else if(statusCode != 204){

                            // Obtain error body message
                            stream = connection.getErrorStream();
                            String errorMessage = getStringBody();

                            // Obtain response message
                            message = connection.getResponseMessage();

                            // Create full error message
                            message += "\n" + errorMessage;
                        }

                        valid = true;
                        this.handler.handleSuccessfulHttpTaskCallback(this);

                        dataRead = true;
                    }
                } catch (EOFException ex) {
                    NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] EOFException occurred with HTTP request "+reqType + taskUrl + " " + ex.getMessage());
                    failures++;
                    if (failures <= MAX_CONNECTIONS) {
                        disconnect(connection);
                        connection = null;
                        this.reset();
                    }else{
                        valid = false;
                        this.handleFailedCase();
                    }
                } catch (IOException ex) {
                    // TODO 03-07-2016 Vince: Handle this exception better, friggin no clue what's going on with each task.
                    valid = false;
                    NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] IOException occurred with HTTP request "+reqType + taskUrl + " " + ex.getLocalizedMessage());
                    this.handleFailedCase();
                    break;
                }
                catch(Exception ex){
                    valid = false;
                    NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] "+ex.getClass().getName()+" occurred with HTTP request " + taskUrl + " " + NxUtils.getStackTraceAsString(ex));
                    this.handleFailedCase();
                    break;
                }
                finally {
                    disconnect(connection);
                }
                

            }
        } else {
            NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] No handler assigned. Unable to run NxHttpTask.");
        }
    }
    
    public InputStream getStream(){
        return stream;
    }
    
    private HttpURLConnection createConnection(boolean forceClose){
        //TODO: check if internet is on?

        HttpURLConnection result = null;
        try {

            String finalUrl = this.taskUrl;
            if(urlParams != null && !urlParams.isEmpty()){
                finalUrl = finalUrl+"?"+NxUtils.urlEncodeUTF8(this.urlParams);
            }

            URL url = new URL(finalUrl);
            //if(this.HttpType.getType(finalUrl)
            result = (HttpURLConnection) url.openConnection();

            if (forceClose) {
                result.setRequestProperty("Connection", "close");
            }

            switch(reqType){
                case POST:
                    result.setRequestMethod("POST");
                    result.setDoInput(true);
                    result.setDoOutput(true);

                    OutputStream os = result.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));

                    writer.write(this.payload);
                    writer.flush();
                    writer.close();
                    os.close();

                    break;
                case DELETE:
                    result.setRequestMethod("DELETE");
                    break;
                case PUT:
                    result.setRequestMethod("PUT");
                    break;
                default:

            }

        } catch (MalformedURLException ex) {
            valid = false;
            NxLogger.error(TAG, "[createConnection] MalformedURLException: "+ex.getMessage());
            this.handleFailedCase();
        } catch (IOException ex) {
            valid = false;
            NxLogger.error(TAG, "[createConnection] IOException"+ex.getMessage());
            this.handleFailedCase();
        }
        
        return result;
    }
    

    private void disconnect(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
        valid = false;
    }

    public String getStringBody() {
        if(statusCode == 0){
            return null;
        }

        if (message == null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder resultBuffer = new StringBuilder();

            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    resultBuffer.append(line);
                }
            } catch (IOException ex) {
                NxLogger.error(TAG, "[getStringBody] "+ex.getMessage());
            }
            return resultBuffer.toString();
        } else {
            return message;
        }
    }
    
    @Override
    public float getPercentStatus() {
        if((System.currentTimeMillis() - lastUpdate) >1000){
            try {
                percentDownloaded = (float)(fileInfo.position()/(float)contentSize);
            } catch (IOException ex) {
                NxLogger.error(TAG,"[getPercentStatus] "+ex.getMessage());
            }
            lastUpdate = System.currentTimeMillis();
        }
        return percentDownloaded;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getContentSize(){ return  contentSize; }

    public Object getCompanionData() {
        return companionData;
    }
  

    public boolean isValid(){
        return valid;
    }
    
    public String getTaskUrl() {
        return taskUrl;
    }

}
