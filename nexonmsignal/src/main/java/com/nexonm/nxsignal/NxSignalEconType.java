package com.nexonm.nxsignal;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public enum NxSignalEconType {
    EARN("earn",1),
    SPEND("spend",2);

    private int intValue;
    private String strValue;

    private NxSignalEconType(String neoStringValue, int neoIntValue){
        intValue = neoIntValue;
        strValue = neoStringValue;
    }

    @Override
    public String toString(){
        return strValue;
    }
    
    public int toInt(){
        return intValue;
    }
}
