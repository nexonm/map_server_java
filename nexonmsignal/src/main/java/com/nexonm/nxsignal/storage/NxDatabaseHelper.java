package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.storage.NxDatabaseContract.NxDatabaseEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Iterator;

import java.text.SimpleDateFormat;

import static com.nexonm.nxsignal.utils.NxUtils.isNullOrEmptyString;

/**
 * Created by giorgiotino on 8/4/17.
 */

public class NxDatabaseHelper {

    //
    // SQL query strings
    //
    private static final String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + NxDatabaseEvent.TABLE_NAME + " (" +
                    NxDatabaseEvent.COLUMN_NAME_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    NxDatabaseEvent.COLUMN_NAME_EVENT_BODY + " TEXT," +
                    NxDatabaseEvent.COLUMN_NAME_EVENT_TYPE + " TEXT," +
                    NxDatabaseEvent.COLUMN_NAME_PRIORITY + " INTEGER," +
                    NxDatabaseEvent.COLUMN_NAME_INSERTED_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    private static final String SQL_DELETE_ENTRIES =
                    "DROP TABLE IF EXISTS " + NxDatabaseEvent.TABLE_NAME;

    //
    // If you change the database schema, you must increment the database version.
    //
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "map_client.db";

    private static final String TAG = "NxDatabaseHelper";

    //
    // Lock to make sure the db is safe. Should not be required ad the DB should already provide
    // some kind of locking by default, but it does not hurt.
    //
    private Object _lock = new Object();
    private long _eventId = -1;
    private HashMap<Long, NxDatabaseRow> _database = null;

    public NxDatabaseHelper() {
        _database = new HashMap<Long, NxDatabaseRow>();
    }

    public synchronized boolean insertEvent(String body, String type, int priority) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String currentTimestamp = sdfDate.format(now);

        NxDatabaseRow databaseRow = new NxDatabaseRow(++_eventId,body,type,priority,currentTimestamp);

        int size;

        synchronized (_lock) {
            _database.put(databaseRow.getEventId(), databaseRow);
            size = _database.size();
        }

        NxLogger.verbose(TAG, "[insertEvent] Last insert event_id: %d, event_type:%s, event_body:%s, priority:%d", databaseRow.getEventId(), type, body, priority);

        return (size > -1);
    }

    public synchronized int countEvents() {

        int numRows = -1;
        
        synchronized (_lock) {
            numRows = _database.size();
        }

        NxLogger.verbose(TAG,"[countEvents] %d events in database.", numRows);

        return numRows;
    }

    public synchronized ArrayList<NxDatabaseRow> selectEvents(String type, int limit) {

        ArrayList<NxDatabaseRow> arrayList = new ArrayList<NxDatabaseRow>();
        
        synchronized (_lock) {
            for (Map.Entry<Long, NxDatabaseRow> entry : _database.entrySet()) {
                Long key = entry.getKey();
                NxDatabaseRow value = entry.getValue();

                if (isNullOrEmptyString(type) || value.getEventType().equalsIgnoreCase(type)) {
                    arrayList.add(value);
                }
            }
        }

        NxLogger.verbose(TAG,"[selectEvents] %d events selected.", arrayList.size());

        return arrayList;
    }

    private static String join(String delimiter, Iterable<? extends Object> objs) {

        Iterator<? extends Object> iter = objs.iterator();
        StringBuilder buffer = new StringBuilder();
        buffer.append(iter.next());
        while (iter.hasNext()) {
            buffer.append(delimiter).append(iter.next());
        }
        return buffer.toString();
    }

    public synchronized boolean deleteEvents(List<Long> eventIds) {
        String inClause = NxDatabaseHelper.join(",", eventIds);
        
        int rowDeletedCount = 0;

        synchronized (_lock) {
            Iterator<Long> it = eventIds.iterator();
            while (it.hasNext()) {
                Long id = (Long) it.next();

                _database.remove(id);

                rowDeletedCount++;
            }
        }

        NxLogger.verbose(TAG,"[deleteEvents] %d events deleted, ids: [ %s ]", rowDeletedCount, inClause);

        return true;
    }

    public synchronized boolean deleteEvents(int limit) {

        int i = 0;
        
        synchronized (_lock) {
            Iterator it = _database.entrySet().iterator();
            while (it.hasNext() && (i < limit)) {
                Map.Entry pair = (Map.Entry) it.next();
                it.remove(); // avoids a ConcurrentModificationException
                i++;
            }
        }

        NxLogger.verbose(TAG,"[deleteEvents] %d events deleted, limit %d", i, limit);

        return true;
    }

    public synchronized void deleteAllEvents() {

        synchronized (_lock) {
            _database.clear();
        }
    }

    public synchronized void clearDatabase() {
        deleteAllEvents();
    }

    public synchronized void deleteDatabase() {

        synchronized (_lock) {
            _database.clear();
            _database = null;
        }
    }
}
