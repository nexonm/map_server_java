/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.logging.NxLogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;

/**
 *
 * @author David.Vallejo
 */
public class NxStorageSaveTask extends NxStorageTask {
    private static final String TAG = "NxStorageSaveTask";
    private File fileHandle;
    private FileOutputStream rawStream;
    private FileChannel fileInfo;
    private float percentSaved;
    private long lastUpdate ;
    private long finalSaveSize;
    private boolean valid = false;
    
    private NxStorageSaveTaskHandler handler;
    
    NxStorageSaveTask(String prefixPath, String path, NxStorageSaveTaskHandler handler) {
        super(StorageTaskType.SAVE, prefixPath, path);
        percentSaved = 0;
        lastUpdate = 0;
        finalSaveSize = 1;//so we don't divide by zero
        this.handler = handler;
    }

    @Override
    public void run() {
        this.forceOneRunOnObjectAtATime();
    }
    
    private synchronized void forceOneRunOnObjectAtATime(){
             
        //TODO: maybe error out on second run?
        if (handler != null) {
            try {
                fileHandle = new File(this.getPath());
                rawStream = new FileOutputStream(fileHandle);
                fileInfo = rawStream.getChannel();
                valid = true;
                handler.handleSuccessfulSaveTaskCallback(this);
            } catch (FileNotFoundException ex) {
                valid = false;
                NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                this.notifyFailed();
            }catch (Exception ex){
                valid = false;
                NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                this.notifyFailed();
            } finally {
                if(this.rawStream != null) {
                    try {
                        this.rawStream.close();
                    } catch (IOException ex) {
                        NxLogger.error(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                    }
                }
            }
        }
    }

    private void notifyFailed(){
        try{
            handler.handleFailedSaveTaskCallback(this);
        }catch (Exception ex){
            NxLogger.error(TAG, "[notifyFailed] " + ex.getMessage());
        }

    }

    @Override
    public float getPercentStatus() {
        //Some Throttleing just in case
        if((System.currentTimeMillis() - lastUpdate) >1000){
            try {
                percentSaved = (float)(fileInfo.position()/finalSaveSize);
            } catch (IOException ex) {
                NxLogger.error(TAG, "[getPercentStatus] " + ex.getMessage());
            }
            lastUpdate = System.currentTimeMillis();
        }
        return percentSaved;
    }
    
    public void saveString(String strToSave){
        if(strToSave != null){
            try {
                byte[] bytesToSave = strToSave.getBytes("UTF8");
                saveBytes(bytesToSave);
            } catch (UnsupportedEncodingException ex) {
                NxLogger.error(TAG, "[saveString] " + ex.getMessage());
            }
        }
    }
    
    public void saveBytes(byte[] bytesToSave){
        if(bytesToSave != null){
            finalSaveSize = bytesToSave.length;
            try {
                rawStream.write(bytesToSave);
            } catch (IOException ex) {
                NxLogger.error(TAG, "[saveBytes] " + ex.getMessage());
            }
        }
        
    }
    
    public OutputStream getRawStream(){
        return this.rawStream;
    }
    
    public boolean isValid(){
        return valid;
    }
    
    
}
