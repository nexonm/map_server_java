/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.queue.NxTaskStatus;

import java.io.File;

/**
 *
 * @author David.Vallejo
 */
public abstract class NxStorageTask implements Runnable, NxTaskStatus {
    
    public enum StorageTaskType{
        SAVE,
        LOAD
    }
    
    private final StorageTaskType type;
    private final String prefixPath;
    private final String localPath;

    
    protected NxStorageTask(StorageTaskType type, String prefixPath, String localPath) {
        this.type = type;
        this.prefixPath = prefixPath;
        this.localPath = localPath;
    }

    public StorageTaskType getType() {
        return type;
    }

    public String getPrefixPath() {

        return prefixPath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getPath() {
        //TODO: fix this to use File.separator
        return prefixPath + "." + localPath;
    }
    
}
