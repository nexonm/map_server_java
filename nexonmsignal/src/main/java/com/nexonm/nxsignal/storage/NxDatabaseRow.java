package com.nexonm.nxsignal.storage;

/**
 * Created by giorgiotino on 8/4/17.
 */

public class NxDatabaseRow {

    private long eventId;
    private String eventBody;
    private String eventType;
    private int priority;
    private String insertedTimestamp;

    public NxDatabaseRow(long eventId, String eventBody, String eventType, int priority, String insertedTimestamp) {
        this.eventId = eventId;
        this.eventBody = eventBody;
        this.eventType = eventType;
        this.priority = priority;
        this.insertedTimestamp = insertedTimestamp;
    }

    public long getEventId() {
        return eventId;
    }

    public String getEventBody() {
        return eventBody;
    }

    public String getEventType() {
        return eventType;
    }

    public int getPriority() {
        return priority;
    }

    public String getInsertedTimestamp() {
        return insertedTimestamp;
    }
}
