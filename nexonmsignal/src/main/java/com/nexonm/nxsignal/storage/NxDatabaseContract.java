package com.nexonm.nxsignal.storage;

/**
 * Created by giorgiotino on 8/4/17.
 */

public final class NxDatabaseContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private NxDatabaseContract() {}

    /* Inner class that defines the table contents */
    public static class NxDatabaseEvent {
        public static final String TABLE_NAME = "event";
        public static final String COLUMN_NAME_EVENT_ID = "event_id";
        public static final String COLUMN_NAME_EVENT_TYPE = "event_type";
        public static final String COLUMN_NAME_EVENT_BODY = "event_body";
        public static final String COLUMN_NAME_PRIORITY = "priority";
        public static final String COLUMN_NAME_INSERTED_TIMESTAMP = "inserted_timestamp";
    }
}