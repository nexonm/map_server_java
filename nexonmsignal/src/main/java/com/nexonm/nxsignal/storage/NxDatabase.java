package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.event.NxEvent;
import com.nexonm.nxsignal.logging.NxLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giorgiotino on 8/4/17.
 */
public class NxDatabase {

    private static final String TAG = "NxDatabase";

    private NxDatabaseHelper databaseHelper = null;

    private static NxDatabase sharedInstance = null;

    public static synchronized NxDatabase getInstance() {
        if (sharedInstance == null){
            sharedInstance = new NxDatabase();
        }
        return sharedInstance;
    }

    private NxDatabase() {}

    public static String getDatabaseName() { return NxDatabaseHelper.DATABASE_NAME; }

    public static int getDatabaseVersion() { return NxDatabaseHelper.DATABASE_VERSION; }

    public boolean isSetup() { return databaseHelper != null; }

    public synchronized void setup() {

        if(databaseHelper == null) {
            databaseHelper = new NxDatabaseHelper();
        }
    }

    public boolean insertEvent(NxEvent event) {
        return this.insertEvent(event.getBody(), event.getType(), event.getPriority());
    }

    public boolean insertEvent(String body, String type, int priority) {
        if(isSetup()) {
            return databaseHelper.insertEvent(body, type, priority);
        } else {
            NxLogger.error(TAG, "[insertEvent] Database is not initialized yet.");
            return false;
        }
    }

    public int countEvents() {
        if(isSetup()) {
            return databaseHelper.countEvents();
        } else {
            NxLogger.error(TAG, "[countEvents] Database is not initialized yet.");
            return -1;
        }
    }

    public ArrayList<NxDatabaseRow> selectEvents(int limit) {
        return this.selectEvents(null, limit);
    }

    public ArrayList<NxDatabaseRow> selectEvents(String type, int limit) {
        if(isSetup()) {
            return databaseHelper.selectEvents(type, limit);
        } else {
            NxLogger.error(TAG, "[selectEvents] Database is not initialized yet.");
            return null;
        }
    }

    public boolean deleteEvents(List<Long> eventIds) {
        if(isSetup()) {
            return databaseHelper.deleteEvents(eventIds);
        } else {
            NxLogger.error(TAG, "[deleteEvents] Database is not initialized yet.");
            return false;
        }
    }

    public boolean deleteEvents(int limit) {
        if(isSetup()) {
            return databaseHelper.deleteEvents(limit);
        } else {
            NxLogger.error(TAG, "[deleteEvents] Database is not initialized yet.");
            return false;
        }
    }

    public boolean deleteAllEvents() {
        if(isSetup()) {
            databaseHelper.deleteAllEvents();
            return true;
        } else {
            NxLogger.error(TAG, "[deleteAllEvents] Database is not initialized yet.");
            return false;
        }
    }

    public boolean clearDatabase() {
        if(isSetup()) {
            databaseHelper.clearDatabase();
            return true;
        } else {
            NxLogger.error(TAG, "[clearDatabase] Database is not initialized yet.");
            return false;
        }
    }

    public boolean deleteDatabase() {
        if(isSetup()) {
            databaseHelper.deleteDatabase();
            databaseHelper = null;
            return true;
        } else {
            NxLogger.error(TAG, "[deleteDatabase] Database is not initialized yet.");
            return false;
        }
    }
}
