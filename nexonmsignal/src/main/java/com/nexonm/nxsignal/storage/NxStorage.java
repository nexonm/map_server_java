/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.networking.NxHttpService;
import com.nexonm.nxsignal.queue.DispatchQueue;
import com.nexonm.nxsignal.queue.NxTaskStatus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by David.Vallejo on 2/3/16.
 */
public class NxStorage {

    public static final String STORAGE_FILE_PATH = "NxStorage";

    private static final String TAG = "NxStorage";
    private static final String DISPATCH_QUEUE_NAME = "io.Nexon.Storage.Queue";

    private static NxStorage sharedInstance = null;

    public static synchronized NxStorage getInstance() {
        if (sharedInstance == null){
            sharedInstance = new NxStorage();
        }
        return sharedInstance;
    }

    public static String appendPathComponent(String base,String addition) {
        return base+"/"+addition;
    }

    private DispatchQueue internalQueue;

    private boolean isSetup = false;

    private String pathOfStorageFile;

    private NxStorage() {
        internalQueue = DispatchQueue.getQueueWithKey(DISPATCH_QUEUE_NAME, DispatchQueue.DrainStrategy.SERIAL);
        internalQueue.setPauseState(false);
    }

    public synchronized boolean setup() {
        if(!isSetup) {
            try {
                pathOfStorageFile = "path-of-storage-file";
                isSetup = true;
            } catch (Exception ex) {
                NxLogger.error(TAG, "[setup] Failed to setup storage: %s", ex.getMessage());
                isSetup = false;
            }
        }
        return isSetup;
    }

    public NxTaskStatus saveToFile(String path, NxStorageSaveTaskHandler handler) {

        NxStorageTask result = new NxStorageSaveTask(pathOfStorageFile, path, handler);
        internalQueue.add(result);

        return result;
    }

    public NxTaskStatus loadFromFile(String path, NxStorageLoadTaskHandler handler, Object companionData) {

        NxStorageTask result = new NxStorageLoadTask(pathOfStorageFile, path, handler,companionData);
        internalQueue.add(result);

        return result;
    }

    public void streamFile(NxHttpService.FileStreamer httpHandler) {

        NxStorageTask fileSaver = new NxStorageSaveTask(pathOfStorageFile, httpHandler.getSavePath(), httpHandler);
        httpHandler.setStorageTask(fileSaver);
    }

    //
    // Following methods are legacy code to load synchronously a JSONObject from the storage files.
    // There are required for backward compatibility after the NxPersistentStorage change.
    //
    public String getStorageFilePath(String localPath) {
        return pathOfStorageFile + "." + localPath;
    }

    public boolean fileExists(String path) {

        File file = new File(this.getStorageFilePath(path));

        return file.exists();
    }

    public String loadStringFromFile(String path, String encoding) {

        String contentString = null;

        FileInputStream rawStream = null;

        try {
            File fileHandle = new File(this.getStorageFilePath(path));
            rawStream = new FileInputStream(fileHandle);

            InputStreamReader input;
            if(encoding == null){
                input = new InputStreamReader(rawStream);
            }else{
                input = new InputStreamReader(rawStream, encoding);
            }

            BufferedReader fileBuffer = new BufferedReader(input);

            StringBuilder resultBuffer = new StringBuilder();
            String line;

            while ((line = fileBuffer.readLine()) != null) {
                resultBuffer.append(line);
            }

            contentString = resultBuffer.toString();
        } catch (FileNotFoundException ex) {

            NxLogger.warn(TAG, "[loadJSONObjectFromFile] " + ex.getMessage());

        } catch (UnsupportedEncodingException e) {

            NxLogger.error(TAG, "[loadJSONObjectFromFile] " + e.getMessage());

        } catch (Exception ex){

            NxLogger.warn(TAG, "[loadJSONObjectFromFile] " + ex.getMessage());

        } finally {

            if(rawStream != null) {
                try {
                    rawStream.close();
                } catch (IOException ex) {
                    NxLogger.warn(TAG, "[loadJSONObjectFromFile] " + ex.getMessage());
                }
            }
        }

        return contentString;
    }

    public boolean deleteFile(String path) {

        String storageFilePath = getStorageFilePath(path);

        NxLogger.info(TAG,"[deleteFile] Attempting to delete file: %s", storageFilePath);

        File fileToDelete = new File(getStorageFilePath(path));
        boolean result = fileToDelete.delete();

        if(result) {
            NxLogger.info(TAG,"[deleteFile] File %s succesfully deleted.", storageFilePath);
        } else {
            NxLogger.error(TAG,"[deleteFile] File %s could not be deleted.", storageFilePath);
        }

        return result;
    }
}