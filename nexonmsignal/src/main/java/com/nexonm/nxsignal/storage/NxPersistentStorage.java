package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.logging.NxLogger;

import java.util.Set;
import java.util.HashMap;

/**
 * Created by giorgiotino on 7/18/17.
 */

public class NxPersistentStorage {

    public static final String PERSISTENT_STORAGE_NAME = "NxPersistentStorage";

    private static final String TAG = "NxPersistentStorage";

    final HashMap<String, Object> sharedPreferences = new HashMap<String, Object>();
    boolean isSetup = false;

    private static NxPersistentStorage sharedInstance = null;

    public static synchronized NxPersistentStorage getInstance() {
        if (sharedInstance == null){
            sharedInstance = new NxPersistentStorage();
        }
        return sharedInstance;
    }

    private NxPersistentStorage() {
    }

    public synchronized boolean setup() {

        if(!isSetup) {
            isSetup = true;
        }

        return isSetup;
    }

    public boolean isSetup() { return isSetup; }

    public boolean has(String key) {
        if(!isSetup || (sharedPreferences == null) || (key == null)) {
            return false;
        }

        return sharedPreferences.containsKey(key);
    }

    public void putString(String key, String value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putString] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putString] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putString] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void putStringSet(String key, Set<String> value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putStringSet] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putStringSet] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putStringSet] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void putInt(String key, int value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putInt] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putInt] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putInt] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void putBoolean(String key, boolean value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putBoolean] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putBoolean] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putBoolean] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void putLong(String key, long value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putLong] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putLong] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putLong] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void putFloat(String key, float value) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.put(key, value);

                NxLogger.verbose(TAG, "[putFloat] Value stored in persistent storage: " + key + " => " + value);
            } else {
                NxLogger.warn(TAG, "[putFloat] Failed to store value in persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[putFloat] Failed to store value in persistent storage (isSetup = false)");
        }
    }

    public void remove(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.remove(key);

                NxLogger.verbose(TAG, "[remove] Value removed from persistent storage: " + key);
            } else {
                NxLogger.warn(TAG, "[remove] Failed to remove value from persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[remove] Failed to remove value from persistent storage (isSetup = false)");
        }
    }

    public void clear() {
        if(isSetup) {
            if(sharedPreferences != null) {
                sharedPreferences.clear();

                NxLogger.verbose(TAG, "[clear] Persistent storage cleared.");
            } else {
                NxLogger.warn(TAG, "[clear] Failed to clear persistent storage (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[clear] Failed to clear persistent storage (isSetup = false)");
        }
    }

    public String getString(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                String value = (String)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getString] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getString] Failed to get string value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getString] Failed to get string value (isSetup = false)");
        }

        return null;
    }

    public Set<String> getStringSet(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                Set<String> value = (Set<String>)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getStringSet] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getStringSet] Failed to get value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getStringSet] Failed to get value (isSetup = false)");
        }

        return null;
    }

    public int getInt(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                int value = (int)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getInt] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getInt] Failed to get value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getInt] Failed to get value (isSetup = false)");
        }

        return 0;
    }

    public boolean getBoolean(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                boolean value = (boolean)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getBoolean] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getBoolean] Failed to get value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getBoolean] Failed to get value (isSetup = false)");
        }

        return false;
    }

    public float getFloat(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                float value = (float)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getFloat] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getFloat] Failed to get value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getFloat] Failed to get value (isSetup = false)");
        }

        return 0.0f;
    }

    public long getLong(String key) {
        if(isSetup) {
            if(sharedPreferences != null) {
                long value = (long)sharedPreferences.get(key);

                NxLogger.verbose(TAG,"[getLong] Value returned for key " + key + " : " + value);

                return value;
            } else {
                NxLogger.warn(TAG, "[getLong] Failed to get value (sharedPreferences = null)");
            }
        } else {
            NxLogger.warn(TAG, "[getLong] Failed to get value (isSetup = false)");
        }

        return 0;
    }
}
