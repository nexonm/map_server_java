/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */

package com.nexonm.nxsignal.storage;

import com.nexonm.nxsignal.logging.NxLogger;
import com.nexonm.nxsignal.utils.NxUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;

/**
 *
 * @author David.Vallejo
 */
public class NxStorageLoadTask extends NxStorageTask{
    
    private static final String TAG = "NxStorageLoadTask";
    private File fileHandle;
    private FileChannel fileInfo;
    private FileInputStream rawStream;
    private float percentRead;
    private long lastUpdate;
    private boolean valid = false;
    private String value = null;
    private Object companionData;

    // static?
    private NxStorageLoadTaskHandler handler;

    NxStorageLoadTask(String prefixPath, String path, NxStorageLoadTaskHandler handler,Object companionData) {
        super(StorageTaskType.LOAD, prefixPath, path);
        percentRead = 0;
        lastUpdate = 0;
        this.companionData = companionData;
        
        this.handler = handler;
        
    }

    @Override
    public void run() {
        this.forceOneRunOnObjectAtATime();
    }
    
    private synchronized void forceOneRunOnObjectAtATime(){
        
        //TODO: maybe error out on second run?
        if (handler != null) {
            try {
                NxLogger.verbose(TAG, "----------------------------------------" + this.getPath());
                fileHandle = new File(this.getPath());
                rawStream = new FileInputStream(fileHandle);
                fileInfo = rawStream.getChannel();
                valid = true;
                handler.handleSuccessfulLoadTaskCallback(this);
            } catch (FileNotFoundException ex) {
                valid = false;
                NxLogger.warn(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                this.notifyFailed();
            } catch (Exception ex){
                valid = false;
                NxLogger.warn(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                this.notifyFailed();
            }finally {
                if(this.rawStream != null) {
                    try {
                        this.rawStream.close();
                        valid = false;
                    } catch (IOException ex) {
                        NxLogger.warn(TAG, "[forceOneRunOnObjectAtATime] " + ex.getMessage());
                    }
                }
            }
        }
    }

    private void notifyFailed(){
        try{
            handler.handleFailedLoadTaskCallback(this);
        }catch (Exception ex){
            NxLogger.error(TAG, "[notifyFailed] " + NxUtils.getStackTraceAsString(ex));
        }

    }

    @Override
    public float getPercentStatus() {
        //Some Throttleing just in case
        if((System.currentTimeMillis() - lastUpdate) >1000){
            try {
                percentRead = (float)(fileInfo.position()/fileInfo.size());
            } catch (IOException ex) {
                NxLogger.error(TAG, "[getPercentStatus] " + NxUtils.getStackTraceAsString(ex));
            }
            lastUpdate = System.currentTimeMillis();
        }
        return percentRead;
    }
    
    public String getStringValue(String encoding){
        if(value == null){
            value = this.getStringFromFile(encoding);
        }
        return value;
    }
    /*
    public byte[] getBytesValue(){
        TODO implement getBytesValue
    }
    */
    
    public InputStream getInputStream(){
        return this.rawStream;
    }
   
    private String getStringFromFile(String encoding){
        String result = null;
        try {
            InputStreamReader input;
            if(encoding == null){
                input = new InputStreamReader(this.rawStream);
            }else{
                input = new InputStreamReader(this.rawStream, encoding);
            }
            BufferedReader fileBuffer = new BufferedReader(input);

            StringBuilder resultBuffer = new StringBuilder();
            String line;

            while ((line = fileBuffer.readLine()) != null) {
                resultBuffer.append(line);
            }

            result = resultBuffer.toString();

        } catch (UnsupportedEncodingException e) {
            NxLogger.error(TAG, "[getStringFromFile] " + e.getMessage());
            result = null;
        } catch (FileNotFoundException e) {
            NxLogger.error(TAG, "[getStringFromFile] " + e.getMessage());
            result = null;
        } catch (IOException e) {
            NxLogger.error(TAG, "[getStringFromFile] " + e.getMessage());
            result = null;
        }
        return result;
    }
    
    public boolean isValid(){
        return valid;
    }

    public Object getCompanionData() {
        return companionData;
    }
    
}
