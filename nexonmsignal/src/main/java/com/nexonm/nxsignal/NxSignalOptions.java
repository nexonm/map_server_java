/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public class NxSignalOptions {
    private String environment;
    private boolean outputLoggingToConsole;
    private boolean autoTrackLaunchEvents;
    private String logLevel;
    private String config;

    private String partner_id;
    private String game_id;
    private String service_id;


    public NxSignalOptions() {
        environment = "prod";
        outputLoggingToConsole = true;
        autoTrackLaunchEvents = true;
        logLevel = "info";
        config = null;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public boolean isOutputLoggingToConsole() {
        return outputLoggingToConsole;
    }

    public void setOutputLoggingToConsole(boolean outputLoggingToConsole) {
        this.outputLoggingToConsole = outputLoggingToConsole;
    }

    public boolean isAutoTrackLaunchEvents() {
        return autoTrackLaunchEvents;
    }

    public void setAutoTrackLaunchEvents(boolean autoTrackLaunchEvents) {
        this.autoTrackLaunchEvents = autoTrackLaunchEvents;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getLogLevel() { return logLevel; }

    public void setLogLevel(String logLevel) { this.logLevel = logLevel; }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

}
