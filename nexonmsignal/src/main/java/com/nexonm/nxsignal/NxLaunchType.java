/**
 * Copyright (C) 2016 NEXON M Inc. All rights reserved.
 *
 * This File can not be copied and/or distributed
 * without the permission of NEXON M Inc.
 * Unauthorized copying of this file, via any medium
 * is strictly prohibited.
 * This file is proprietary and confidential.
 */
package com.nexonm.nxsignal;

/**
 * Created by David.Vallejo on 2/2/16.
 */
public enum NxLaunchType{
    APP_OPEN("app_open",1),
    RESUME("resume",2),
    AWAKE("awake",3),
    LOGIN("login",4);

    private int intValue;
    private String strValue;

    private NxLaunchType(String neoStringValue, int neoIntValue){
        intValue = neoIntValue;
        strValue = neoStringValue;
    }

    @Override
    public String toString(){
        return strValue;
    }


}