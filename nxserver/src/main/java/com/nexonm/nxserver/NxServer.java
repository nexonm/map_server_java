package com.nexonm.nxserver;

import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONException;
import org.json.HTTP;

import com.nexonm.nxsignal.NxSignal;
import com.nexonm.nxsignal.NxSignalOptions;
import com.nexonm.nxsignal.config.JsonKeys;
import com.nexonm.nxsignal.utils.NxUtils;

import java.util.Map;
import java.util.HashMap;

import java.io.IOException;
import java.io.BufferedReader;

@WebServlet(
        name = "NxServer", 
        description = "NxServer Game Test",
        urlPatterns = { 
            "/",
            "/create_user",
            "/login",
            "/purchase"},
        loadOnStartup = 1,
        asyncSupported = true)
public class NxServer extends HttpServlet
{
    //
    // Those values are set per game server, probably from a config file. We set them here directly
    // for short...
    //
    final private String appId = "nxserver";
    final private String partnerId = "nexonm";
    final private String nexonDeviceId = "3D96AC3B-DF61-4993-92FB-015FA5BA7108";
    final private String signature = "80b9c444e54c2dd9242d7e32ccd16abec6b5dad1e19292612f5d54597e1442e0";

    private long _developerPlayerId = 12800000000000000L;
    
    private Object purchaseLock = new Object();
    private long _purchase_id = -1;

    private Object databaseLock = new Object();
    private HashMap<String, Object> database;

    public void init(ServletConfig config) throws ServletException {

        super.init(config);

        //
        // This is a very simple way to store a pair of developer_player_id and client metadata.
        // Of course in a real game server it will be implemented in a more robust way.
        //
        database = new HashMap<String, Object>();

        //
        // Setup and start the Nexonm MAP SDK
        //
        try {
            //
            // Signal options
            //
            NxSignalOptions signalOptions = new NxSignalOptions();
            signalOptions.setEnvironment("dev");
            signalOptions.setOutputLoggingToConsole(true);
            signalOptions.setAutoTrackLaunchEvents(true);
            signalOptions.setGame_id("dwu");
            signalOptions.setLogLevel("debug");
    
            String apiKey = "643b61fe-7213-4884-85b8-b7c4776d6a51";
    
            NxSignal.start(apiKey, signalOptions);

            //
            // AFTER starting the SDK, set the server side global default values
            //
            Map<String, Object> globalDefaultValues = new HashMap<String, Object>();
            globalDefaultValues.put("app_id", appId);
            globalDefaultValues.put("partner_id", partnerId);
            globalDefaultValues.put("nexon_device_id", nexonDeviceId);
            globalDefaultValues.put("signature", signature);

            NxSignal.setGlobalDefaultValues(globalDefaultValues);

        } catch (Exception e) {
            getServletContext().log("An exception occurred in NxServer", e);
            throw new ServletException("An exception occurred in NxServer"
                    + e.getMessage());
        }
    }

    //
    // This is just a very simple debug method to easily check if a given developer_player_id has been saved in the database,
    // and to read the client metadata associated to it, if any.
    //
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        String developerPlayerId = request.getParameter("developer_player_id");

        if((developerPlayerId == null) || (developerPlayerId.length() <= 0)) {
            response.getWriter().print("Query parameter developer_player_id is either null or empty.");
        }
        else {
            JSONObject clientMetadata = null;

            synchronized(databaseLock) {
                if(database.containsKey(developerPlayerId)) {
                    clientMetadata = (JSONObject)database.get(developerPlayerId);
                }
            }

            if(clientMetadata != null) {
                response.getWriter().print(clientMetadata);
            } else {
                response.getWriter().print("Cannot find developer_player_id=" + developerPlayerId + " in the database.");
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String path = request.getServletPath(); 

        if(path.endsWith("create_user")) {
            createUser(request, response);
        } else if(path.endsWith("login")) {
            login(request, response);
        } else if (path.endsWith("purchase")) {
            purchase(request, response);
        }
    }

    private JSONObject parseJsonBody(HttpServletRequest request) {
        String data = "";   
        StringBuilder builder = new StringBuilder();

        try {
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        data = builder.toString();

        JSONObject object = new JSONObject();
        
        try {
            object = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

    private void copyParameter(String key, JSONObject from, JSONObject to) {
        if(from.has(key)) { to.put(key, from.get(key)); }
    }

    private JSONObject parseClientMetadata(JSONObject jsonBody) {
        JSONObject clientMetadata = new JSONObject();

        this.copyParameter("app_build_version", jsonBody, clientMetadata);
        this.copyParameter("os_name", jsonBody, clientMetadata);
        this.copyParameter("partner_id", jsonBody, clientMetadata);
        this.copyParameter("os_platform", jsonBody, clientMetadata);
        this.copyParameter("device_make", jsonBody, clientMetadata);
        this.copyParameter("sdk_version", jsonBody, clientMetadata);
        this.copyParameter("device_type", jsonBody, clientMetadata);
        this.copyParameter("app_locale", jsonBody, clientMetadata);
        this.copyParameter("platform_device_id", jsonBody, clientMetadata);
        this.copyParameter("platform_device_id_type", jsonBody, clientMetadata);
        this.copyParameter("toy_service_id", jsonBody, clientMetadata);
        this.copyParameter("npsn", jsonBody, clientMetadata);
        this.copyParameter("session_id", jsonBody, clientMetadata);

        return clientMetadata;
    }

    private boolean validateReceipt(JSONObject receipt) {
        //
        // In the real world, the server will probably touch base with the Apple Store or Google Play
        // to make sure that the receipt is valid. 
        //
        boolean isValid = false;

        isValid |= receipt.has("orderId");
        isValid |= receipt.has("packageName");
        isValid |= receipt.has("productId");
        isValid |= receipt.has("purchaseTime");
        isValid |= receipt.has("purchaseState");
        isValid |= receipt.has("developerPayload");
        isValid |= receipt.has("purchaseToken");
        isValid |= receipt.has("price");
        isValid |= receipt.has("price_currency_code");
        isValid |= receipt.has("price_amount_micros");

        return isValid;
    }

    //
    // createUser is called when the client does NOT have a developer_player_id yet. It will generate one
    // and store the client metadata received, using the newly generated developer_player_id as key.
    //
    private void createUser(HttpServletRequest request, HttpServletResponse response) {
    
        JSONObject jsonBody = parseJsonBody(request);
        JSONObject clientMetadata = parseClientMetadata(jsonBody);

        String developerPlayerId = null;

        //
        // For the sake of simplicity, we assume that a call to createUser will always create a new
        // user, even if the client has already a developer_player_id set.
        //
        synchronized(databaseLock) {
            //
            // Create a new developer_player_id
            //
            developerPlayerId = String.valueOf(++_developerPlayerId);

            //
            // Save the client metadata for this developer_player_id
            //
            database.put(developerPlayerId, clientMetadata);
        }

        //
        // Add the newly created developer_player_id to the overwriteParameters (a Map version of the JSON object)
        //
        Map<String, Object> overwriteParameters = NxUtils.convertJSONObjectToMap(clientMetadata);

        overwriteParameters.put("developer_player_id",developerPlayerId);
        
        //
        // Prepare the client response
        //
        response.setContentType("application/json");

        JSONObject responseJsonObject = new JSONObject();

        //
        // Return the newly created developer_player_id
        //
        responseJsonObject.put("developer_player_id", developerPlayerId);
        responseJsonObject.put("success", true);

        try {
            response.getWriter().print(responseJsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //
        // Add the developer_player_id to the overwriteParameters (A.K.A. clientMetadata)
        //
        overwriteParameters.put("developer_player_id", developerPlayerId);

        //
        // Send a user_creation_server event containing the client metadata too.
        //
        NxSignal.reportCustomEvent("user_creation_server", overwriteParameters, new HashMap<String, Object>());
    }

    //
    // login is called when the client already has a developer_player_id. Client metadata stored for
    // the given developer_player_id are UPDATED with the new client metadata set received.
    //
    private void login(HttpServletRequest request, HttpServletResponse response) {
        
        JSONObject jsonBody = parseJsonBody(request);
        JSONObject clientMetadata = parseClientMetadata(jsonBody);
        
        //
        // Setup the response
        //
        response.setContentType("application/json");

        JSONObject responseJsonObject = new JSONObject();

        //
        // Try to retrieve the developer_player_id from the JSON body
        //
        String developerPlayerId = null;

        if(jsonBody.has("developer_player_id")) {
            developerPlayerId = jsonBody.getString("developer_player_id");
        }

        //
        // If the client did not send the developer_player_id, then it cannot login...
        //
        if((developerPlayerId == null) || (developerPlayerId.length() <= 0)) {
            //
            // Return and error message
            //
            responseJsonObject.put("success", false);
            responseJsonObject.put("error_message", "developer_player_id missing, cannot login!");
        }
        else {

            boolean canLogin = false;

            synchronized(databaseLock) {
                //
                // If the client metadata for the given developer_player_id can be found...
                //
                if(database.containsKey(developerPlayerId)) {
                    //
                    // ...update the client metadata for this developer_player_id.
                    //
                    database.put(developerPlayerId, clientMetadata);

                    canLogin = true;
                } else {
                    //
                    // The client is trying to login with an unknown developer_player_id - cannot login, must
                    // create_user first!
                    //
                    canLogin = false;
                }
            }

            //
            // Prepare the client response
            //
            response.setContentType("application/json");

            if(canLogin) {
                //
                // Return an error message
                //
                responseJsonObject.put("success", true);

            } else {        
                //
                // Return an error message
                //
                responseJsonObject.put("success", false);
                responseJsonObject.put("error_message", "developer_player_id unknown, cannot login!");
            }
        }

        try {
            response.getWriter().print(responseJsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void purchase(HttpServletRequest request, HttpServletResponse response) {
        
        JSONObject jsonBody = parseJsonBody(request);

        //
        // Setup the response
        //
        response.setContentType("application/json");
        
        JSONObject responseJsonObject = new JSONObject();

        //
        // Here we do not expect the client to send along the client metadata again, so we just
        // retrieve the client metadata stored in the database for the given developer_player_id.
        //
        String developerPlayerId = null;
        
        if(jsonBody.has("developer_player_id")) {
            developerPlayerId = jsonBody.getString("developer_player_id");
        }
        
        //
        // If the client did not send the developer_player_id, then it cannot purchase...
        //
        if((developerPlayerId == null) || (developerPlayerId.length() <= 0)) {
            //
            // Return and error message
            //
            responseJsonObject.put("success", false);
            responseJsonObject.put("error_message", "developer_player_id missing, cannot login!");
        } else {

            JSONObject clientMetadata = null;
            boolean canPurchase = false;
            boolean validReceipt = false;

            synchronized(databaseLock) {
                //
                // If the client metadata for the given developer_player_id can be found...
                //
                if(database.containsKey(developerPlayerId)) {
                    //
                    // ...get the client metadata for this developer_player_id.
                    //
                    clientMetadata = (JSONObject)database.get(developerPlayerId);

                    //
                    // Validate the receipt the receipt and stuff...
                    // 
                    validReceipt = validateReceipt(jsonBody);

                    canPurchase = validReceipt;
                } else {
                    //
                    // The client is trying to purchase with an unknown developer_player_id
                    //
                    canPurchase = false;
                }
            }

            if(canPurchase) {

                long purchaseId;

                synchronized(purchaseLock) {
                    purchaseId = ++_purchase_id;
                }

                responseJsonObject.put("success", true);
                responseJsonObject.put("purchase_id", purchaseId);

                Map<String, Object> overwriteParameters = NxUtils.convertJSONObjectToMap(clientMetadata);

                //
                // Add the developer_player_id to the overwriteParameters (A.K.A. clientMetadata)
                //
                overwriteParameters.put("developer_player_id", developerPlayerId);

                NxSignal.reportCustomEvent("purchase_server", overwriteParameters, new HashMap<String, Object>());
            } else {
                responseJsonObject.put("success", false);
            }
        }

        try {
            response.getWriter().print(responseJsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}