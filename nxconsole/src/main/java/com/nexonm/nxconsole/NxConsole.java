package com.nexonm.nxconsole;

import com.nexonm.nxsignal.NxSignal;
import com.nexonm.nxsignal.NxSignalOptions;

import java.util.HashMap;

public class NxConsole
{
	public static void main(String[] args) {

        System.out.println(System.getProperty("java.home"));

        NxSignalOptions signalOptions = new NxSignalOptions();
        signalOptions.setEnvironment("dev");
        signalOptions.setOutputLoggingToConsole(true);
        signalOptions.setAutoTrackLaunchEvents(true);
        signalOptions.setGame_id("dwu");
        signalOptions.setLogLevel("debug");

        String apiKey = "643b61fe-7213-4884-85b8-b7c4776d6a51";

        NxSignal.start(apiKey, signalOptions);
        
        NxSignal.reportCustomEvent("custom", new HashMap<String, Object>(), new HashMap<String, Object>());
	}
}